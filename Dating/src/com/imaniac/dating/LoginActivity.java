package com.imaniac.dating;

import java.util.Timer;
import java.util.TimerTask;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.dd.processbutton.iml.ActionProcessButton;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.imaniac.application.DatingApplication;
import com.imaniac.fragments.ImageFragment;
import com.imaniac.fragments.LoginFragment;
import com.imaniac.transform.ZoomOutPageTransformer;


public class LoginActivity extends FragmentActivity  {
	public  FragmentTransaction ft ;
	Fragment newFragment;
	public int currentimageindex=0;
	Timer timer;
	TimerTask task;
	RadioGroup rg;
	RadioButton rb0,rb1,rb2,rb3,rb4,rb[];
	
	private static final int NUM_PAGES = 5;

    /**
     * The pager widget, which handles animation and allows swiping horizontally to access previous
     * and next wizard steps.
     */
    private ViewPager mPager;

    /**
     * The pager adapter, which provides the pages to the view pager widget.
     */
    private PagerAdapter mPagerAdapter;
	//ImageView slidingimage;
	ActionProcessButton signIn,signUp;
	public static int opt_selected=2;
	
	private int[] IMAGE_IDS = {
			R.drawable.splash0, R.drawable.splash1, R.drawable.splash2,
			R.drawable.splash3,R.drawable.splash4
		};
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_login);
		
		mPager = (ViewPager) findViewById(R.id.pager);
        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        //mPager.setPageTransformer(true, new ZoomOutPageTransformer());
        mPager.setAdapter(mPagerAdapter);
		mPager.setOnPageChangeListener(new OnPageChangeListener() {
			
			@Override
			public void onPageSelected(int arg0) {
				// TODO Auto-generated method stub
				currentimageindex=arg0;
				rg.clearCheck();
				switch(arg0)
				{
				case 0:
					rb0.setChecked(true);
					rb0.setClickable(false);
					break;
				case 1:
					rb1.setChecked(true);
					rb1.setClickable(false);
					break;
				case 2:
					rb2.setChecked(true);
					rb2.setClickable(false);
					break;
				case 3:
					rb3.setChecked(true);
					rb3.setClickable(false);
					break;
				case 4:
					rb4.setChecked(true);
					rb4.setClickable(false);
					break;
				}
				
			}
			
			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		rg=(RadioGroup)findViewById(R.id.radioGroup1);
		rb0=(RadioButton)findViewById(R.id.radio0);
		rb1=(RadioButton)findViewById(R.id.radio1);
		rb2=(RadioButton)findViewById(R.id.radio2);
		rb3=(RadioButton)findViewById(R.id.radio3);
		rb4=(RadioButton)findViewById(R.id.radio4);
		
		Log.e("uid received", ""+DatingApplication.sp.getString("uid", "abc"));
		if(DatingApplication.sp.getString("uid", "abc").length()==32)
		{
			Intent i = new Intent(LoginActivity.this,MainActivity.class);
			MainActivity.verification_time=0;
			finish();
			startActivity(i);
		}
		signIn=(ActionProcessButton)findViewById(R.id.signIn);
		signUp=(ActionProcessButton)findViewById(R.id.signUp);
		signIn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				newFragment = new LoginFragment();
				ft= getSupportFragmentManager().beginTransaction();
		        ft.add(android.R.id.content, newFragment).commit();
				opt_selected=0;
			}
		});
		signUp.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				newFragment = new LoginFragment();
				ft= getSupportFragmentManager().beginTransaction();
		        ft.add(android.R.id.content, newFragment).commit();
		        opt_selected=1;
			}
		});
		DatingApplication.getApp().setLoginAct(this);
		final Handler mHandler = new Handler();

        // Create runnable for posting
        final Runnable mUpdateResults = new Runnable() {
            public void run() {
            	currentimageindex++;
            	AnimateandSlideShow();
            	
            }
        };
		
        int delay = 0; // delay for 1 sec.

        int period = 5000; // repeat every 4 sec.

        Timer timer = new Timer();

        timer.scheduleAtFixedRate(new TimerTask() {

        public void run() {

        	 mHandler.post(mUpdateResults);

        }

        }, delay, period);
        
    	((DatingApplication) getApplication()).getTracker(DatingApplication.TrackerName.APP_TRACKER);

		//localstoragehandler.check_unread_messages(DatingApplication.sp.getString("uid", "abc"));
		/*if (dbCursor.getCount() > 0)
		DatingApplication.getService().showNotification(DatingApplication.profile_uid,"abc","Abc");*/
	}
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		GoogleAnalytics.getInstance(this).reportActivityStart(this);
	}
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		GoogleAnalytics.getInstance(this).reportActivityStop(this);
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		
		/*try
		{
			if(opt_selected==0 || opt_selected==1)
			{
				ft.detach(newFragment);
			}
			else
				finish();
		}catch(Exception e)
		{
			finish();
		}*/
		if(opt_selected==0 || opt_selected==1)
		{
			try{
			Log.e("Entered","Entered");
			getSupportFragmentManager().beginTransaction().
			remove(getSupportFragmentManager().findFragmentById(android.R.id.content)).commit();
			ft.remove(newFragment);
			opt_selected=2;
			}catch(Exception e)
			{
				
			}
			
		}
		else
		{	//finish();
		super.onBackPressed();
		opt_selected=2;
		}
	}
	 private void AnimateandSlideShow() {
	    	
	    	
	    	
	    
	   		
	   		mPager.setCurrentItem(currentimageindex%5);
	        
	    }
	 private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
	        public ScreenSlidePagerAdapter(FragmentManager fm) {
	            super(fm);
	        }

	        @Override
	        public Fragment getItem(int position) {
	            return new ImageFragment(position);
	        }

	        @Override
	        public int getCount() {
	            return NUM_PAGES;
	        }
	    }

}
