package com.imaniac.dating;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.ActionBar;
import android.app.Activity;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.imaniac.application.DatingApplication;
import com.imaniac.databases.LocalStorageHandler;

import com.imaniac.service.OnlineService;


public class MessagingActivity extends Activity {
	private static final int MESSAGE_CANNOT_BE_SENT = 0;
	public String username,msg;
	private EditText messageText;
	private ListView messageHistoryText;
	List<String> messages=new ArrayList<String>();
	List<Integer>gravity = new ArrayList<Integer>();
	ChatArrayAdapter adapter;
	//private EditText messageHistoryText;
	private Button sendMessageButton;
	public static ActionBar actionBar;
	//private FriendInfo friend = new FriendInfo();
	private LocalStorageHandler localstoragehandler; 
	private Cursor dbCursor;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.messaging_screen);
		
		actionBar = getActionBar();
		actionBar.show();
		actionBar.setDisplayHomeAsUpEnabled(false);
		actionBar.setDisplayShowHomeEnabled(false);
		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setIcon(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
		actionBar.setDisplayUseLogoEnabled(false);
		actionBar.setCustomView(R.layout.layout_action_bar2);
		ImageButton mHomeButton = (ImageButton)actionBar.getCustomView().findViewById(R.id.home_button);
		
		mHomeButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
		
		messageHistoryText = (ListView) findViewById(R.id.list);
		
		messageText = (EditText) findViewById(R.id.message);
		
		messageText.requestFocus();			
		
		sendMessageButton = (Button) findViewById(R.id.sendMessageButton);
		
		Bundle extras = this.getIntent().getExtras();
		try
		{
			
			OnlineService.friend_uid=extras.getString("friend");
			OnlineService.friend_name=extras.getString("name");
			//actionBar.setTitle(OnlineService.friend_name);
			actionBar.setDisplayShowTitleEnabled(true);
			Log.e("intent received", ""+extras.getString("name") +" "+OnlineService.friend_uid);
			
		}catch(Exception e)
		{
			
		}
		DatingApplication.getApp().setMessageAct(this);
		actionBar.setTitle("  "+OnlineService.friend_name);
		
	//	EditText friendUserName = (EditText) findViewById(R.id.friendUserName);
	//	friendUserName.setText(friend.userName);
		
		
		localstoragehandler = new LocalStorageHandler(this);
		dbCursor = localstoragehandler.get(OnlineService.friend_uid, DatingApplication.sp.getString("uid", "abc") );
		
		if (dbCursor.getCount() > 0){
		int noOfScorer = 0;
		if(dbCursor.getCount()>20)
			dbCursor.moveToPosition(dbCursor.getCount()-20);
		else
			dbCursor.moveToFirst();
		    while ((!dbCursor.isAfterLast())&&noOfScorer<dbCursor.getCount()) 
		    {
		        noOfScorer++;

				this.appendToMessageHistory(dbCursor.getString(4) , dbCursor.getString(5));
		        dbCursor.moveToNext();
		    }
		}
		
		((NotificationManager)getSystemService(NOTIFICATION_SERVICE)).cancel(101);
		OnlineService.noti_count=0;
		if (msg != null) 
		{
			this.appendToMessageHistory(OnlineService.friend_name , msg);
			//((NotificationManager)getSystemService(NOTIFICATION_SERVICE)).cancel((OnlineService.friend_uid+msg).hashCode());
		}
		adapter=new ChatArrayAdapter(getApplicationContext(), R.layout.activity_chat_singlemessage);
		messageHistoryText.setAdapter(adapter);
		localstoragehandler.change_read_status(OnlineService.friend_uid, DatingApplication.profile_uid);
        
		adapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                messageHistoryText.setSelection(adapter.getCount() - 1);
                localstoragehandler.change_read_status(OnlineService.friend_uid, DatingApplication.profile_uid);
            }
        });
		sendMessageButton.setOnClickListener(new OnClickListener(){
			CharSequence message;
			Handler handler = new Handler();
			public void onClick(View arg0) {
				message = messageText.getText();
				if (message.length()>0) 
				{		
					appendToMessageHistory("Me", message.toString());
					
					localstoragehandler.insert(DatingApplication.sp.getString("uid", "abc"),"Me", OnlineService.friend_uid,OnlineService.friend_name, message.toString());
							new SendMessage().execute(messageText.getText().toString());	
							localstoragehandler.insert_friend(OnlineService.friend_name, OnlineService.friend_uid);
					messageText.setText("");
					
										
				}
				
			}});
		
		messageText.setOnKeyListener(new OnKeyListener(){
			public boolean onKey(View v, int keyCode, KeyEvent event) 
			{
				if (keyCode == 66){
					sendMessageButton.performClick();
					return true;
				}
				return false;
			}
			
			
		});
		localstoragehandler.close();
		((DatingApplication) getApplication()).getTracker(DatingApplication.TrackerName.APP_TRACKER);

	}
	
	

	@Override
	protected void onPause() {
		super.onPause();
		super.onPause();
		try{
			DatingApplication.getService().setasregistered("offline");
		}catch(Exception e)
		{
			
		}
		
	}
@Override
protected void onStart() {
	// TODO Auto-generated method stub
	super.onStart();
	GoogleAnalytics.getInstance(this).reportActivityStart(this);
	LocalBroadcastManager.getInstance(MessagingActivity.this).registerReceiver(mMessageReceiver, new IntentFilter("messages"));

}
@Override
protected void onStop() {
	// TODO Auto-generated method stub
	super.onStop();
	GoogleAnalytics.getInstance(this).reportActivityStop(this);
	LocalBroadcastManager.getInstance(MessagingActivity.this).unregisterReceiver(mMessageReceiver);
}
	@Override
	protected void onResume() 
	{		
		super.onResume();
		Intent mIntent = new Intent(this, OnlineService.class);
        //bindService(mIntent, mConnection, BIND_AUTO_CREATE);
		try{
			DatingApplication.getService().setasregistered("online");
		}catch(Exception e)
		{
			
		}	
		
		
	}
	
	
	public class  MessageReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) 
		{		
			Bundle extra = intent.getExtras();
			String username = extra.getString("user");			
			String message = extra.getString("msg");
			
			if (username != null && message != null)
			{
				if (OnlineService.friend_uid.equals(username)) {
					appendToMessageHistory(OnlineService.friend_name, message);
					localstoragehandler.insert(username,OnlineService.friend_name,DatingApplication.sp.getString("uid", "abc"),"Me", message);
					
				}
				else {
					if (message.length() > 15) {
						message = message.substring(0, 15);
					}
					Toast.makeText(MessagingActivity.this,  username + " says '"+
													message + "'",
													Toast.LENGTH_SHORT).show();		
				}
			}
			adapter=new ChatArrayAdapter(getApplicationContext(), R.layout.activity_chat_singlemessage);
			messageHistoryText.setAdapter(adapter);
			//update();
		}
		
	};
	private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
	  	  @Override
	  	  public void onReceive(Context context, Intent intent) {
	  	    // Extract data included in the Intent
	  	    
	  	   Log.e("receiver", "called");
	  	 Bundle extra = intent.getExtras();
			String username = extra.getString("user");			
			String message = extra.getString("msg");
			
			if (username != null && message != null)
			{
				if (OnlineService.friend_uid.equals(username)) {
					appendToMessageHistory(OnlineService.friend_name, message);
					localstoragehandler.insert(username,OnlineService.friend_name,DatingApplication.sp.getString("uid", "abc"),"Me", message);
					
				}
				else {
					if (message.length() > 15) {
						message = message.substring(0, 15);
					}
					Toast.makeText(MessagingActivity.this,  username + " says '"+
													message + "'",
													Toast.LENGTH_SHORT).show();		
				}
			}
			adapter=new ChatArrayAdapter(getApplicationContext(), R.layout.activity_chat_singlemessage);
			Log.e("adapter", "updated");
			messageHistoryText.setAdapter(adapter);
			messageHistoryText.getLastVisiblePosition();
	  	   // adapter.setNotifyOnChange(true);
	  	   // adapter.notifyDataSetChanged();
	  	   
	  	    
	  	  }
	  	};
	
	public  void appendToMessageHistory(String username, String message) {
		if (username != null && message != null) {
			messages.add(message);
			if(username.contains("Me"))
				gravity.add(1);
			else
				gravity.add(0);
			//messageHistoryText.append(username + ":\n");								
			//messageHistoryText.append(message + "\n");
		}
	}
	
	
	@Override
	protected void onDestroy() {
	    super.onDestroy();
	    if (localstoragehandler != null) {
	    	localstoragehandler.close();
	    }
	    if (dbCursor != null) {
	    	dbCursor.close();
	    }
	}

	
	private class SendMessage extends AsyncTask<String, Void, String> 
	{ 
		@Override 
		protected String doInBackground(String... params) 
		
		{ 
			try
			{
			String Url = "http://imaniacgroup.com/api/dating/putMessage.php?sender="+DatingApplication.sp.getString("uid", "abc")+"&receiver="+OnlineService.friend_uid+"&msg="+URLEncoder.encode(params[0]);
					Log.e("post url is ", Url);
			
		    StringBuilder response = new StringBuilder();
		    try {
		        HttpPost post = new HttpPost();
		        post.setURI(new URI(Url));
		        
		        DefaultHttpClient httpClient = new DefaultHttpClient();
		        HttpResponse httpResponse = httpClient.execute(post);
		        if (httpResponse.getStatusLine().getStatusCode() == 200) {
		            Log.d("post", "HTTP POST succeeded");
		            HttpEntity messageEntity = httpResponse.getEntity();
		            InputStream is = messageEntity.getContent();
		            BufferedReader br = new BufferedReader(new InputStreamReader(is));
		            String line;
		            while ((line = br.readLine()) != null) {
		                response.append(line);
		            }
		        } else {
		            Log.e("post", "HTTP POST status code is not 200 but "+httpResponse.getStatusLine().getStatusCode());
		        }
		    } catch (Exception e) {
		        Log.e("post", e.getMessage());
		    }
		    Log.d("post", "Done with HTTP posting");
		    Log.e("post resonse is ", response.toString());
		    return response.toString();
			}catch(Exception e)
			{
				
			}

			 return null;
		} 
		@Override
		protected void onPostExecute(String data) { 
			Intent intent = new Intent("messages");
			  // add data
			  intent.putExtra("message", "data");
			LocalBroadcastManager.getInstance(MessagingActivity.this).sendBroadcast(intent);
			} 
		}
	
	public  void update()
	{
		localstoragehandler = new LocalStorageHandler(OnlineService.mContext);
		dbCursor = localstoragehandler.get(OnlineService.friend_uid, DatingApplication.sp.getString("uid", "abc") );
		
		if (dbCursor.getCount() > 0){
		int noOfScorer = 0;
		//messageHistoryText.setText("");
		if(dbCursor.getCount()>20)
			dbCursor.moveToPosition(dbCursor.getCount()-20);
		else
			dbCursor.moveToFirst();
		    while ((!dbCursor.isAfterLast())&&noOfScorer<dbCursor.getCount()) 
		    {
		        noOfScorer++;

				appendToMessageHistory(dbCursor.getString(4) , dbCursor.getString(5));
		        dbCursor.moveToNext();
		    }
		}
		localstoragehandler.close();
	}

	public class ChatArrayAdapter extends ArrayAdapter {

		private TextView chatText;
		
		private LinearLayout singleMessageContainer;

		

		public ChatArrayAdapter(Context context, int textViewResourceId) {
			super(context, textViewResourceId);
		}
		public int getCount() {
			return messages.size();
		}
		

		

		public View getView(int position, View convertView, ViewGroup parent) {
			View row = convertView;
			if (row == null) {
				LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				row = inflater.inflate(R.layout.activity_chat_singlemessage, parent, false);
			}
			singleMessageContainer = (LinearLayout) row.findViewById(R.id.singleMessageContainer);
			
			chatText = (TextView) row.findViewById(R.id.singleMessage);
			chatText.setText(messages.get(position));
			chatText.setBackgroundResource(gravity.get(position)==1? R.drawable.msg2 : R.drawable.msg1);
			singleMessageContainer.setGravity(gravity.get(position)==0? Gravity.LEFT : Gravity.RIGHT);
			return row;
		}

		public Bitmap decodeToBitmap(byte[] decodedByte) {
			return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
		}

	}

}
