package com.imaniac.dating;



import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;

import com.imaniac.api.UnapprovedFriends;
import com.imaniac.application.DatingApplication;
import com.imaniac.fragments.SampleListFragment2.SampleAdapter;
import com.imaniac.service.OnlineService;
import com.imaniac.types.FriendInfo;


import android.app.ActionBar;
import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class UnapprovedFriendList extends Activity {
	GridView friends;
	SampleAdapter adapter ;
	public static ActionBar actionBar;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_unapproved_friend_list);
		friends=(GridView)findViewById(R.id.gridView1);
		
		adapter= new SampleAdapter(this);
		//Log.e("setting", "setting");
		actionBar = getActionBar();
		actionBar.show();
		actionBar.setDisplayHomeAsUpEnabled(false);
		actionBar.setDisplayShowHomeEnabled(false);
		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setIcon(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
		actionBar.setDisplayUseLogoEnabled(false);
		actionBar.setCustomView(R.layout.layout_action_bar2);
		ImageButton mHomeButton = (ImageButton)actionBar.getCustomView().findViewById(R.id.home_button);
		
		mHomeButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
		UnapprovedFriends uf=new UnapprovedFriends();
		NotificationManager nm=(NotificationManager)getSystemService(NOTIFICATION_SERVICE);
		nm.cancel(300);
		try {
			uf.search("abc");
		} catch (ClientProtocolException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (URISyntaxException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if(DatingApplication.unapprovedFriends.size()==0)
		{
			RelativeLayout rl = (RelativeLayout)findViewById(R.id.realtive_layout);
			rl.setBackgroundResource(R.drawable.default_friends);
		}
		else
		{
			
		
			try{
				for (int i = 0; i<DatingApplication.unapprovedFriends.size(); i++) {
					adapter.add(new SampleItem(DatingApplication.unapprovedFriends.get(i).getName(), android.R.drawable.ic_menu_search));
				}
				friends.setAdapter(adapter);
			}catch(Exception e)
			{
				Log.e("list view 1", "exception"+e.toString());
			}
		}
	}

	private class SampleItem {
		public String tag;
		public int iconRes;
		public SampleItem(String tag, int iconRes) {
			this.tag = tag; 
			this.iconRes = iconRes;
		}
	}

	public class SampleAdapter extends ArrayAdapter<SampleItem> {

		public SampleAdapter(Context context) {
			super(context, 0);
		}

		public View getView(final int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				convertView = LayoutInflater.from(getContext()).inflate(R.layout.layout_unapproved_item, null);
			}
			
			
			
			ImageView icon = (ImageView) convertView.findViewById(R.id.row_icon);
			//icon.setImageResource(getItem(position).iconRes);
			TextView name = (TextView)convertView.findViewById(R.id.name);
			name.setText(DatingApplication.unapprovedFriends.get(position).getName());
			TextView details = (TextView)convertView.findViewById(R.id.details);
			//details.setText(DatingApplication.unapprovedFriends.get(position).getAge());
			ImageButton approve=(ImageButton)convertView.findViewById(R.id.add_friend);
			ImageButton decline=(ImageButton)convertView.findViewById(R.id.decline_friend);
			approve.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					
					DatingApplication.getService().change_friend_status(DatingApplication.unapprovedFriends.get(position).getUid(), "approved");
					Toast.makeText(getApplicationContext(), "You have successfully added "+DatingApplication.unapprovedFriends.get(position).getName() + " as your friend"	,3000).show();
					
					DatingApplication.unapprovedFriends.remove(position);
					adapter= new SampleAdapter(getApplicationContext());
					try{
						for (int i = 0; i<DatingApplication.unapprovedFriends.size(); i++) {
							adapter.add(new SampleItem(DatingApplication.unapprovedFriends.get(i).getName(), android.R.drawable.ic_menu_search));
						}
						friends.setAdapter(adapter);
						}catch(Exception e)
					{
						Log.e("list view 2", "exception"+e.toString());
					}
				}
			});
			decline.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					
					DatingApplication.getService().change_friend_status(DatingApplication.unapprovedFriends.get(position).getUid(), "rejected");
					DatingApplication.unapprovedFriends.remove(position);
					adapter= new SampleAdapter(getApplicationContext());
					try{
						for (int i = 0; i<DatingApplication.unapprovedFriends.size(); i++) {
							adapter.add(new SampleItem(DatingApplication.unapprovedFriends.get(i).getName(), android.R.drawable.ic_menu_search));
						}
						friends.setAdapter(adapter);
					}catch(Exception e)
					{
						Log.e("list view 3", "exception"+e.toString());
					}
				}
			});
			return convertView;
		}

	}
	
}
