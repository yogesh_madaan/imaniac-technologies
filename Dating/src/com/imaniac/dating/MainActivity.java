package com.imaniac.dating;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.graphics.Canvas;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.imaniac.api.GetFriends;
import com.imaniac.application.DatingApplication;
import com.imaniac.databases.LocalStorageHandler;
import com.imaniac.fragments.ChatFragment;
import com.imaniac.fragments.SampleListFragment;
import com.imaniac.fragments.SampleListFragment2;
import com.imaniac.fragments.WallFragment;
import com.imaniac.service.OnlineService;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu.CanvasTransformer;

public class MainActivity extends BaseActivity {
	private static final String PROPERTY_ID = "UA-55492190-1";

	
	//FrameLayout fm ;
	public static SharedPreferences sp;
	public static Editor e;
	public static Fragment mContent;
	public static Context mContext;
	public static ActionBar actionBar;
	public ImageButton mHomeButton,mChatButton,mMessageButton;
	boolean mBounded;
	OnlineService mService;
	JSONObject obj1;
	String sid;
	public static int active_fragment,verification_time=200;
	//public static Context mContext;
	private LocalStorageHandler localstoragehandler; 
	private Cursor dbCursor;
	public MainActivity() {
		super(R.string.changing_fragments);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
	
		if (savedInstanceState != null)
			mContent = getSupportFragmentManager().getFragment(savedInstanceState, "mContent");
		if (mContent == null)
			{
				mContent = new WallFragment();
				
				//getSlidingMenu().showMenu();
			}
		Bundle extras = this.getIntent().getExtras();
		try{
			if(extras!=null)
				switchContent(new ChatFragment());
		}catch(Exception e)
		{
			
		}
		DatingApplication.getApp().setAct(this);
		startService(new Intent(MainActivity.this,OnlineService.class));
		mService=DatingApplication.getService();
		mContext=this;
		sp=getSharedPreferences("com.imaniac.dating", Context.MODE_PRIVATE);
		e=sp.edit();
		mContext=this;
		GetFriends frnd=new GetFriends();
		try {
			frnd.search("abc");
		} catch (ClientProtocolException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (URISyntaxException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		DatingApplication.getAct().update_friends();
		actionBar = getActionBar();
		actionBar.show();
		actionBar.setDisplayHomeAsUpEnabled(false);
		actionBar.setDisplayShowHomeEnabled(false);
		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setIcon(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
		actionBar.setDisplayUseLogoEnabled(false);
		actionBar.setCustomView(R.layout.layout_action_bar);
		mHomeButton = (ImageButton)actionBar.getCustomView().findViewById(R.id.home_button);
		mChatButton = (ImageButton)actionBar.getCustomView().findViewById(R.id.chat_button);
		mHomeButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				toggle();
			}
		});
		mChatButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				toggle2();
			}
		});
		
		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM
		        | ActionBar.DISPLAY_SHOW_HOME);
		/*getSupportFragmentManager()
		.beginTransaction()
		.replace(R.id.content_frame, mContent)
		.commitAllowingStateLoss();*/
		// customize the SlidingMenu
		getSlidingMenu().setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
		setSlidingActionBarEnabled(false);
		getSlidingMenu().setBehindScrollScale(0.0f);
		/*getSlidingMenu().setBehindCanvasTransformer(new CanvasTransformer()
		{
			@Override
			public void transformCanvas(Canvas canvas, float percentOpen) {
				float scale = (float) (percentOpen*0.25 + 0.75);
				canvas.scale(scale, scale, canvas.getWidth()/2, canvas.getHeight()/2);
			}
		});*/
		getSlidingMenu().setMode(SlidingMenu.LEFT_RIGHT);
		// set the Above View
		setContentView(R.layout.content_frame);
		getSupportFragmentManager()
		.beginTransaction()
		.replace(R.id.content_frame, mContent)
		.commitAllowingStateLoss();
		
		
		setBehindContentView(R.layout.menu_frame);
		getSupportFragmentManager()
		.beginTransaction()
		.replace(R.id.menu_frame, new SampleListFragment())
		.commit();
		
		getSlidingMenu().setSecondaryMenu(R.layout.menu_frame_two);
		getSlidingMenu().setSecondaryShadowDrawable(R.drawable.shadowright);
		getSupportFragmentManager()
		.beginTransaction()
		.replace(R.id.menu_frame_two, new SampleListFragment2())
		.commit();
		
		/*ApprovedFriends app = new ApprovedFriends();
		try {
			List<FriendInfo> l=app.search("abc");
		} catch (ClientProtocolException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (URISyntaxException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		localstoragehandler = new LocalStorageHandler(this);
		((DatingApplication) getApplication()).getTracker(DatingApplication.TrackerName.APP_TRACKER);

		//localstoragehandler.check_unread_messages(DatingApplication.sp.getString("uid", "abc"));
		/*if (dbCursor.getCount() > 0)
		DatingApplication.getService().showNotification(DatingApplication.profile_uid,"abc","Abc");*/
	}
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		((DatingApplication) getApplication()).getTracker(DatingApplication.TrackerName.APP_TRACKER);

		GoogleAnalytics.getInstance(this).reportActivityStart(this);
	}
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		GoogleAnalytics.getInstance(this).reportActivityStop(this);
	}
	@Override
	public void onSaveInstanceState(Bundle outState) {
		
		//getSupportFragmentManager().putFragment(outState, "mContent", mContent);
		super.onSaveInstanceState(outState);
	}
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		Log.e("activity", "paused");
		try{
			mService.setasregistered("offline");
		}catch(Exception e)
		{
			
		}
		
		
	}
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		Log.e("activity", "resumed");
		Intent mIntent = new Intent(this, OnlineService.class);
        //bindService(mIntent, mConnection, BIND_AUTO_CREATE);
		try{
			mService.setasregistered("online");
		}catch(Exception e)
		{
			
		}
	}
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		Log.e("activity", "destroyed");
	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		//super.onBackPressed();
		if(active_fragment==1)
		{
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				MainActivity.mContext);
 
			// set title
			alertDialogBuilder.setTitle("Dating");
 
			// set dialog message
			alertDialogBuilder
				.setMessage("Do you Really Want To Exit?")
				.setCancelable(false)
				.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						// if this button is clicked, close
						// current activity
						finish();
						//DatingApplication.e.putString("uid", "logout");
						//DatingApplication.e.commit();
					}
				  })
				.setNegativeButton("No",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing
						//DatingApplication.getService().change_friend_status(uid, "rejected");
						dialog.cancel();
					}
				});
 
				// create alert dialog
				AlertDialog alertDialog = alertDialogBuilder.create();
 
				// show it
				alertDialog.show();
		}
		else
			DatingApplication.getAct().switchContent(new WallFragment());
	}
	
	/*@Override
	 protected void onStart() {
	  super.onStart();
	  Intent mIntent = new Intent(this, OnlineService.class);
	        bindService(mIntent, mConnection, BIND_AUTO_CREATE);
	 };
	 
	 ServiceConnection mConnection = new ServiceConnection() {
	  
	  public void onServiceDisconnected(ComponentName name) {
	   Toast.makeText(MainActivity.this, "Service is disconnected", 1000).show();
	   mBounded = false;
	   mService = null;
	  }
	  
	  public void onServiceConnected(ComponentName name, IBinder service) {
	   Toast.makeText(MainActivity.this, "Service is connected", 1000).show();
	   mBounded = true;
	   LocalBinder mLocalBinder = (LocalBinder)service;
	   mService = mLocalBinder.getServerInstance();
	  }
	 };
	 
	 @Override
	 protected void onStop() {
	  super.onStop();
	  if(mBounded) {
	   unbindService(mConnection);
	   mBounded = false;
	  }
	 };*/
	
	public void switchContent(Fragment fragment) {
		try{
			mContent = fragment;
		
		getSupportFragmentManager()
		.beginTransaction()
		.replace(R.id.content_frame, fragment)
		.commit();
		getSlidingMenu().showContent();
		}catch(Exception e)
		{
			
		}
	}

	
	
	public void addView()
	{
		setBehindContentView(R.layout.menu_frame);
		getSupportFragmentManager()
		.beginTransaction()
		.replace(R.id.menu_frame, new SampleListFragment())
		.commitAllowingStateLoss();
		
		getSlidingMenu().setSecondaryMenu(R.layout.menu_frame_two);
		getSlidingMenu().setSecondaryShadowDrawable(R.drawable.shadowright);
		getSupportFragmentManager()
		.beginTransaction()
		.replace(R.id.menu_frame_two, new SampleListFragment2())
		.commitAllowingStateLoss();
		
		
	}
/*public void login()
{
	 Fragment newFragment = new LoginFragment();
     FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
      ft.add(android.R.id.content, newFragment).commit();

}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}*/
	public void update_friends()
	{try{
		/*getSlidingMenu().setSecondaryMenu(R.layout.menu_frame_two);
		getSlidingMenu().setSecondaryShadowDrawable(R.drawable.shadowright);
		getSupportFragmentManager()
		.beginTransaction()
		.replace(R.id.menu_frame_two, new SampleListFragment2())
		.commitAllowingStateLoss();*/
		Intent i = new Intent("visibility");
		
		LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(i);
			 
	}catch(Exception e)
	{}
	}
	public void setasregistered()
	{
		Log.e("setting", "registered");
		DatingApplication.e.putString("reg_status", "registered");
		DatingApplication.e.commit();
		new SignInTask().execute();
	}
	private class SignInTask extends AsyncTask<String, Void, String> 
	{ 
		@Override 
		protected String doInBackground(String... params) 
		
		{ 
			String Url = "http://imaniacgroup.com/api/dating/updateUserProfile.php?uid="+DatingApplication.sp.getString("uid", "abc")+"&status=registered";
			Log.e("sign url is ", Url);
			
		    StringBuilder response = new StringBuilder();
		    try {
		        HttpPost post = new HttpPost();
		        post.setURI(new URI(Url));
		        
		        DefaultHttpClient httpClient = new DefaultHttpClient();
		        HttpResponse httpResponse = httpClient.execute(post);
		        if (httpResponse.getStatusLine().getStatusCode() == 200) {
		            Log.d("post", "HTTP POST succeeded");
		            HttpEntity messageEntity = httpResponse.getEntity();
		            InputStream is = messageEntity.getContent();
		            BufferedReader br = new BufferedReader(new InputStreamReader(is));
		            String line;
		            while ((line = br.readLine()) != null) {
		                response.append(line);
		            }
		        } else {
		            Log.e("post", "HTTP POST status code is not 200 but "+httpResponse.getStatusLine().getStatusCode());
		        }
		    } catch (Exception e) {
		        Log.e("post", e.getMessage());
		    }
		    Log.d("post", "Done with HTTP posting");
		    Log.e("post resonse is ", response.toString());
		    
		    try {
		    	obj1 = new JSONObject(response.toString());
				
				sid=obj1.getString("__imaniac_api__");
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				
				
				e.printStackTrace();
			}
		    
		   // return response.toString();
		

			 return null;
		} 
		@Override
		protected void onPostExecute(String data) { 
			try{
				
				if(sid.equalsIgnoreCase("success"))
			
			{
					DatingApplication.e.putString("status", "registered");
					DatingApplication.e.commit();
					Handler handler=new Handler();
				    handler=  new Handler(getMainLooper());
				    handler.post( new Runnable(){
				        public void run(){
				        	
									Toast.makeText(MainActivity.this, "Phone No Verified ",Toast.LENGTH_LONG).show();
									
				        
				        }
				    });			}
				
				
			}catch(Exception e)
			{
				Log.e("exception is ",""+ e.toString());
				
			}
			
			
			} 
		}
	
	
	
	

}
