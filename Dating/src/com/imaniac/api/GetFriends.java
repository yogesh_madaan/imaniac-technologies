package com.imaniac.api;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.StrictMode;
import android.util.Log;


import com.imaniac.application.DatingApplication;
import com.imaniac.dating.LoginActivity;
import com.imaniac.types.FriendInfo;

public class GetFriends {
	 public static List<FriendInfo> search(String query) throws ClientProtocolException, IOException, URISyntaxException, JSONException {
	        List<FriendInfo> ListData = new ArrayList<FriendInfo>();
	        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
	        String name=null,status = null,on_stat=null,uid=null,read="0",friend_status;
	        StrictMode.setThreadPolicy(policy); 
	        DatingApplication.approvedFriends=new ArrayList<FriendInfo>();
	        DatingApplication.unapprovedFriends=new ArrayList<FriendInfo>();
	       // String temp = URLEncoder.encode(query, "UTF-8") ;
	        String Url="http://imaniacgroup.com/api/dating/getUnapprovedFriends.php?uid="+DatingApplication.sp.getString("uid", "abc");
			
			URI url=new URI(Url);
			
			HttpClient client=new DefaultHttpClient();
			
			HttpGet get=new HttpGet();
			get.setURI(url);
			
			HttpResponse response=client.execute(get);
			
			HttpEntity entity=response.getEntity();
			String s = null;
			
				s=EntityUtils.toString(entity);
				JSONObject obj=new JSONObject(s);
				//Log.e("return friends data", ""+obj);
				JSONArray array=obj.getJSONArray("__imaniac_api__");
				
				
				
				for(int i=0;i<array.length();i++)
				{
					
					//Log.e("element ", ""+array.getString(i));
					//uid=array.getString(i);
					JSONObject ob = array.getJSONObject(i);
					uid=ob.getString("uid");
					name=ob.getString("name");
					status=ob.getString("status");
					on_stat=ob.getString("on_stat");
					read=ob.getString("readit");
					friend_status=ob.getString("reg_status");
					//Log.e("reg_status", friend_status);
					DatingApplication.uids.add(uid);
					if(status.equalsIgnoreCase("approved"))
						DatingApplication.approvedFriends.add(new FriendInfo(name, on_stat, friend_status,uid, "google.com",read));
					else if (status.equalsIgnoreCase("unapproved")&&ob.getString("sender").equalsIgnoreCase("d"))
						DatingApplication.unapprovedFriends.add(new FriendInfo(name, on_stat,friend_status,uid, "google.com",read));
					
					
				}
				
				
				
				
			//Log.e("size is :", ""+DatingApplication.unapprovedFriends.size());
	        return ListData;
	    }
	
}
