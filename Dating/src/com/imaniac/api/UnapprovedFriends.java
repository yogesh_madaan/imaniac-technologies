package com.imaniac.api;

import java.io.IOException;
import java.net.URISyntaxException;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;

import android.app.NotificationManager;
import android.os.StrictMode;
import android.util.Log;

import com.imaniac.application.DatingApplication;

public class UnapprovedFriends {
	public static String name=null,prf_stat = null,on_stat=null,uid=null;
	static boolean show_noti=false;
	 public static void search(String query) throws ClientProtocolException, IOException, URISyntaxException, JSONException {
	       
		 StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
	        
	        StrictMode.setThreadPolicy(policy); 
	        NotificationManager mNM=(NotificationManager) DatingApplication.getAct().getSystemService(DatingApplication.getAct().NOTIFICATION_SERVICE);
	        // Send the notification.
	        // We use a layout id because it is a unique number.  We use it later to cancel.
	        mNM.cancel(300);
	       // Log.e("Unapproved Friends ", "calling" + DatingApplication.unapprovedFriends.size());
		 try{
				
				
				
				
				if(DatingApplication.unapprovedFriends.size()>0)
				{
					//Log.e("notification", "called");
					
				}

				for(int i=0;i<DatingApplication.unapprovedFriends.size();i++)
				{
					
					
					if(DatingApplication.unapprovedFriends.get(i).getRead().equals("0"))
						show_noti=true;
					else
						show_noti=false;
					uid=DatingApplication.unapprovedFriends.get(i).getUid();
					name=DatingApplication.unapprovedFriends.get(i).getName();
					on_stat=DatingApplication.unapprovedFriends.get(i).getOnline_status();
				
				
						//ListData.add(new FriendInfo(name, on_stat, uid, "google.com"));
						
						/*AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
								OnlineService.mContext);
				 
							// set title
							alertDialogBuilder.setTitle("New Friend Request");
				 
							// set dialog message
							alertDialogBuilder
								.setMessage("Do you Want To Add "+ name +" as your friend?")
								.setCancelable(false)
								.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,int id) {
										// if this button is clicked, close
										// current activity
										DatingApplication.getService().change_friend_status(uid, "approved");
										dialog.cancel();
									}
								  })
								.setNegativeButton("No",new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,int id) {
										// if this button is clicked, just close
										// the dialog box and do nothing
										DatingApplication.getService().change_friend_status(uid, "rejected");
										dialog.cancel();
									}
								});
				 
								// create alert dialog
								AlertDialog alertDialog = alertDialogBuilder.create();
				 
								// show it
								alertDialog.show();
					*/
					
				}
				if(show_noti)
					DatingApplication.getService().showFriendNotification();
				
				}catch(Exception e)
				{
					Log.e("unapproved exception", ""+e.toString());
				}
		 
		
	    }
	
	
}
