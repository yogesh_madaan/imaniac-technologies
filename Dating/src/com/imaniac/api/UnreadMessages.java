package com.imaniac.api;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.os.StrictMode;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.imaniac.application.DatingApplication;
import com.imaniac.databases.LocalStorageHandler;
import com.imaniac.dating.LoginActivity;
import com.imaniac.dating.MainActivity;
import com.imaniac.dating.MessagingActivity;
import com.imaniac.fragments.Messaging;
import com.imaniac.service.OnlineService;
import com.imaniac.types.FriendInfo;
import com.imaniac.types.MessageInfo;

public class UnreadMessages {

	public static List<MessageInfo> search(String query) throws ClientProtocolException, IOException, URISyntaxException, JSONException {
        List<MessageInfo> ListData = new ArrayList<MessageInfo>();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        String name=null,msg=null,uid=null;
        LocalStorageHandler localstoragehandler =new LocalStorageHandler(OnlineService.mContext); 
        StrictMode.setThreadPolicy(policy); 
   
       // String temp = URLEncoder.encode(query, "UTF-8") ;
        String Url="http://imaniacgroup.com/api/dating/getMessages.php?receiver="+DatingApplication.sp.getString("uid", "abc");
		//Log.e("unreaad message", Url);
		URI url=new URI(Url);
		
		HttpClient client=new DefaultHttpClient();
		
		HttpGet get=new HttpGet();
		get.setURI(url);
		
		HttpResponse response=client.execute(get);
		
		HttpEntity entity=response.getEntity();
		String s = null;
		
			s=EntityUtils.toString(entity);
			JSONObject obj=new JSONObject(s);
			//Log.e("return data", ""+s);
			JSONArray array=obj.getJSONArray("__imaniac_api__");
			
			
			
			for(int i=0;i<array.length();i++)
			{
				
				//Log.e("element ", ""+array.getString(i));
				//uid=array.getString(i);
				JSONObject ob = array.getJSONObject(i);
				uid=ob.getString("sender");
				name=ob.getString("name");
				msg=ob.getString("msg");
				//Messaging.update();
				localstoragehandler.insert(uid,name,DatingApplication.sp.getString("uid", "abc"),"Me", msg);
				localstoragehandler.insert_friend(OnlineService.friend_name, OnlineService.friend_uid);
				if(uid.equalsIgnoreCase(OnlineService.friend_uid))
				{
					//Messaging.appendToMessageHistory(OnlineService.friend_uid, msg);
					Intent intent = new Intent("messages");
					  // add data
					  intent.putExtra("user", OnlineService.friend_uid);
					  intent.putExtra("message", msg);
					  LocalBroadcastManager.getInstance(OnlineService.mContext).sendBroadcast(intent);
				}
				else
				{
					DatingApplication.getService().showNotification(name,uid, msg);
				}
					ListData.add(new MessageInfo(msg, uid, name));
				
				
			}
			
			
			
			
		//Log.e("size is :", ""+ListData.size());
			DatingApplication.getMessageMAct().update();
			Intent intent = new Intent("messages");
			  // add data
			  intent.putExtra("message", "data");
			LocalBroadcastManager.getInstance(DatingApplication.getService().mContext).sendBroadcast(intent);
        return ListData;
    }
}
