package com.imaniac.api;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.StrictMode;
import android.util.Log;

import com.imaniac.application.DatingApplication;
import com.imaniac.dating.LoginActivity;
import com.imaniac.types.FriendInfo;
import com.imaniac.types.UserInfo;

public class RegisteredUser {
	 public static List<UserInfo> search(String query) throws ClientProtocolException, IOException, URISyntaxException, JSONException {
		 List<UserInfo>ListData = new ArrayList<UserInfo>();
		 	        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
	        String name=null,prf_stat = null,on_stat=null,uid=null,age="18";
	        StrictMode.setThreadPolicy(policy); 
	   
	       // String temp = URLEncoder.encode(query, "UTF-8") ;
	        String Url="http://imaniacgroup.com/api/dating/getRegisteredUsers.php";
			
			URI url=new URI(Url);
			
			HttpClient client=new DefaultHttpClient();
			
			HttpGet get=new HttpGet();
			get.setURI(url);
			
			HttpResponse response=client.execute(get);
			
			HttpEntity entity=response.getEntity();
			String s = null;
			
				s=EntityUtils.toString(entity);
				JSONObject obj=new JSONObject(s);
				JSONArray array=obj.getJSONArray("__imaniac_api__");
				
				//Log.e("my uid is" ,DatingApplication.sp.getString("uid", "abc"));

				for(int i=0;i<array.length();i++)
				{
					
					Log.e("element ", ""+array.getString(i));
					//uid=array.getString(i);
					JSONObject ob = array.getJSONObject(i);
					uid=ob.getString("uid");
					name=ob.getString("name");
					age=ob.getString("age");
				
					
					
					if(!uid.equalsIgnoreCase(DatingApplication.sp.getString("uid", "abc")))
					{
						
						ListData.add(new UserInfo(name, uid, age));
						
					}
					
				}
				
				
				
				
			Log.e("registered user list size", ""+ListData.size());
	        return ListData;
	    }
}
