package com.imaniac.fragments;



import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.CallLog;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.dd.processbutton.iml.ActionProcessButton;
import com.imaniac.application.DatingApplication;
import com.imaniac.dating.MainActivity;
import com.imaniac.dating.R;
import com.imaniac.service.GPSTracker;
import com.imaniac.types.WallMessage;
import com.imaniac.utils.ProgressGenerator;


public class WallFragment extends Fragment implements OnScrollListener ,com.imaniac.utils.ProgressGenerator.OnCompleteListener ,OnItemSelectedListener{
	
	ListView listview;
	TextView no_connection;
	LinearLayout btmfrg;
	private int lastposition;
	String[] metro_stations,latitudes_stations,longitudes_stations;
	String sid;
	JSONObject obj1,temp;
	Button discover;
	ProgressGenerator progressGenerator;
	static ActionProcessButton post;
	EditText status;
	Spinner station;
	RelativeLayout status_layout;
	public static ProgressDialog pDialog;
	public static final String API_KEY = "0004-4a7d38c1-536b4a7b-3093-3a849e81";
	public static final String PRIVATE_KEY = "private=0009-4a7d38c1-536b4a7b-309c-50c9d751";
	public static final String DOMAIN = "imaniac.org";
	public List<WallMessage> statuses ;
	TextView default_message;
	List<String> stations_list=new ArrayList<String>();
	SampleAdapter adapter;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		ViewGroup rootView=(ViewGroup)inflater.inflate(R.layout.layout_fragment_wall, container,false);
		listview = (ListView)rootView.findViewById(R.id.listview);
		no_connection=(TextView)rootView.findViewById(R.id.noconnection);
		station=(Spinner)rootView.findViewById(R.id.station);
		status=(EditText)rootView.findViewById(R.id.status_message);
		progressGenerator= new ProgressGenerator(this);
		post=(ActionProcessButton)rootView.findViewById(R.id.post_button);
		metro_stations = getResources().getStringArray(R.array.countries_array);
		latitudes_stations=getResources().getStringArray(R.array.latitudes_array);
		longitudes_stations=getResources().getStringArray(R.array.longitudes_array);
		ArrayAdapter<String> aa = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, metro_stations);
		station.setAdapter(aa);
		status_layout=(RelativeLayout)rootView.findViewById(R.id.status);
		listview.setOnScrollListener(this);
		statuses=new ArrayList<WallMessage>();
		default_message=(TextView)rootView.findViewById(R.id.textView1);
		btmfrg=(LinearLayout)rootView.findViewById(R.id.ll);
		discover=(Button)rootView.findViewById(R.id.discover);
		status_layout.setVisibility(ViewGroup.VISIBLE);
		Double lat1 = Double.parseDouble(DatingApplication.sp.getString("latitude", "0.0"));
		Double long1= Double.parseDouble(DatingApplication.sp.getString("longitude", "0.0"));
		Double min_dist=313131.00;
		
		int min_pos = 0;
		Double temp;
		Log.e("lat is "+lat1, "long is "+long1);
		for(int i=0;i<metro_stations.length;i++)
		{
			temp =DatingApplication.getService().distance(lat1, long1, Double.parseDouble(latitudes_stations[i]),Double.parseDouble(longitudes_stations[i]));
			//Log.e(metro_stations[i], ""+temp+" km");
			//temp=Math.abs((lat1-Double.parseDouble(latitudes_stations[i])));
			//Log.e(metro_stations[i] +"  "+ latitudes_stations[i], ""+temp+" km");
			if(temp<min_dist)
				{
					min_dist=temp;
					min_pos=i;
				}
		}
		Log.e("min pos", metro_stations[min_pos]);
		station.setSelection(min_pos);
		//metro_stations = getResources().getStringArray(R.array.countries_array);
		discover.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
				DatingApplication.getAct().switchContent(new Discover());
			}
		});
		post.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Log.e("post ", "clicked");
				if(status.getText().toString().trim().length()>0)
					{
					ConnectivityManager cm =
	        		        (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
	        		    NetworkInfo netInfo = cm.getActiveNetworkInfo();
	        		    if (netInfo != null && netInfo.isConnectedOrConnecting()) {
	        		    	
	                        progressGenerator.start(post);
	                        post.setEnabled(false);
	                        //btnSignIn.setEnabled(false);
	                        new PostWallMessages().execute();
							
	        		    }
	        		    else
	        		    {
	        		    	Toast.makeText(getActivity(), "Not Connected to Internet", 2000).show();
	        		    }
						
					}
				else
					Toast.makeText(getActivity(), "Enter Some Text ", 3000).show();
			}
		});
		return rootView;
	}
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		//if(LoginActivity.sp.getString("status", "unregistered")!="registered")
			if(MainActivity.verification_time==0)
				new GetUserProfile().execute();
			MainActivity.active_fragment=1;
			stations_list=Arrays.asList(metro_stations);
			new GetWallMessages().execute();
			listview.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {
					// TODO Auto-generated method stub
					DatingApplication.view_profile_uid=statuses.get(arg2).getUid();
					DatingApplication.getAct().switchContent(new ViewProfile());
				}
			});
			Timer timer = new Timer();
			timer.schedule(new TimerTask() {
				
				@Override
				public void run() {
					// TODO Auto-generated method stub
					new GetWallMessages().execute();
				}
			}, 5000,5000);
			try{	
				 GPSTracker gpsTracker = new GPSTracker(getActivity());
					if (gpsTracker.canGetLocation())
			        {
			            String latitude = String.valueOf(gpsTracker.latitude);
			           
			            String longitude = String.valueOf(gpsTracker.longitude);
			            DatingApplication.e.putString("latitide", latitude);
			            DatingApplication.e.putString("longitude", longitude);
			            DatingApplication.e.commit();
			            Log.e("lat "+latitude, "long "+longitude);
			           // Log.e("postal", ""+gpsTracker.getAddressLine(getApplicationContext()));
			        }
			}catch(Exception e)
			{
				Log.e("gps", "exception" + e);
			}
			try{
				//Log.e("messages", ""+statuses.size() );
				
					//setDefault();
				adapter =new SampleAdapter(getActivity(), R.layout.layout_wall_messages,statuses);
			adapter.notifyDataSetChanged();
			if(statuses.size()>0)
				hide();
				listview.setAdapter(adapter);
				
			}catch(Exception e)
			{
				Log.e("view", "exception");
			}
	}
	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,int visibleItemCount, int totalItemCount) {
		 int currentPosition = view.getFirstVisiblePosition();
	        if(currentPosition > lastposition) {
	            //scroll down
	        	//Log.e("msg", "down");
	        btmfrg.setVisibility(View.GONE);
	        	
	        }
	        if(currentPosition < lastposition) {
	            //scroll up
	        	//Log.e("msg", "up");
	        	 btmfrg.setVisibility(View.VISIBLE);
	        }
	        lastposition = currentPosition;
	}
	@Override
	public void onScrollStateChanged(AbsListView arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}
	
	private class GetUserProfile extends AsyncTask<String, Void, String> 
	{ 
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			
		} 
		
		@Override 
		protected String doInBackground(String... params) 
		
		{ 
			try{
			//Log.e("verfiy", "dialog");
			 String Url="http://imaniacgroup.com/api/dating/getUserProfile.php?uid="+DatingApplication.sp.getString("uid", "abc");
			
				URI url = null;
				try {
					url = new URI(Url);
				} catch (URISyntaxException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				HttpClient client=new DefaultHttpClient();
				
				HttpGet get=new HttpGet();
				get.setURI(url);
				
				HttpResponse response = null;
				try {
					response = client.execute(get);
				} catch (ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				HttpEntity entity=response.getEntity();
				String s = null;
				
					try {
						s=EntityUtils.toString(entity);
						//Log.e("s ", s);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					JSONObject obj = null;
					try {
						obj = new JSONObject(s);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					JSONArray array = null;
					try {
						array = obj.getJSONArray("__imaniac_api__");
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						Log.e("exp", "called");
						e.printStackTrace();
					}
					
					

					for(int i=0;i<array.length();i++)
					{
						
						// Log.e("checking user profile", Url);
						try {
							JSONObject ob = array.getJSONObject(i);
							DatingApplication.e.putString("name", ob.getString("name"));
							DatingApplication.e.putString("phone", ob.getString("phone"));
							DatingApplication.e.putString("reg_status", ob.getString("status"));
							DatingApplication.e.commit();
							//Log.e("status received is ", ""+ob.getString("status"));
							if((ob.getString("status").equalsIgnoreCase("unregistered")))
							{
								final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
										getActivity());
						 
									// set title
									alertDialogBuilder.setTitle("Phone No Verification");
						 
									// set dialog message
									alertDialogBuilder
										.setMessage("Do you Want to verify your number now?")
										.setCancelable(false)
										.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
											public void onClick(DialogInterface dialog,int id) {
												// if this button is clicked, close
												// current activity
												Timer timer = new Timer();
												timer.schedule(new TimerTask() {
													
													@Override
													public void run() {
														// TODO Auto-generated method stub
														pDialog.cancel();
														new SendHttpPostTask().cancel(true);
														new SendHttpRequestTask().cancel(true);
														getActivity().runOnUiThread(new Runnable() {
															
															@Override
															public void run() {
																// TODO Auto-generated method stub
																DatingApplication.getService().print_toast("Couldn't Verify your number right now. Try Again later.");
																
															}
														});
														}
												}, 30000);
												new SendHttpRequestTask().execute();
											}
										  })
										.setNegativeButton("No",new DialogInterface.OnClickListener() {
											public void onClick(DialogInterface dialog,int id) {
												// if this button is clicked, just close
												// the dialog box and do nothing
												dialog.cancel();
											}
										});
						 
										// create alert dialog
									DatingApplication.getAct().runOnUiThread(new Runnable() {
										
										@Override
										public void run() {
											// TODO Auto-generated method stub
											AlertDialog alertDialog = alertDialogBuilder.create();
											 
											// show it
											alertDialog.show();
											MainActivity.verification_time=200;
										}
									});
										
							}
							else
								Log.e("status", "registered");
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							Log.e("json exception ", e.toString());
							e.printStackTrace();
						}
						
						
						
					}
					
					
					
					
			}catch(Exception e)
			{
				
			}

			 return null;
		} 
		@Override
		protected void onPostExecute(String data) { 
			
		}
	}
	private class SendHttpRequestTask extends AsyncTask<String, Void, String> 
	{ 
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			pDialog=new ProgressDialog(getActivity());
			pDialog.setTitle("Please Wait");
			// Set progressbar message
			pDialog.setMessage("Verifying your Mobile No ");
			pDialog.setIndeterminate(false);
			//pDialog.setCancelable(false);
			pDialog.setCanceledOnTouchOutside(false);
			// Show progressbar
			pDialog.show();
		} 
		
		@Override 
		protected String doInBackground(String... params) 
		
		{ 
			try{
				
			
			String Url = "https://api.mOTP.in/v1/"+API_KEY+"/91"+DatingApplication.sp.getString("phone", "000");
			Log.e("url is ", Url);
			StringBuilder response = new StringBuilder();
		    try {
		        HttpGet get = new HttpGet();
		        get.setURI(new URI(Url));
		        DefaultHttpClient httpClient = new DefaultHttpClient();
		        HttpResponse httpResponse = httpClient.execute(get);
		        if (httpResponse.getStatusLine().getStatusCode() == 200) {
		            Log.d("demo", "HTTP Get succeeded");

		            HttpEntity messageEntity = httpResponse.getEntity();
		            InputStream is = messageEntity.getContent();
		            BufferedReader br = new BufferedReader(new InputStreamReader(is));
		            String line;
		            while ((line = br.readLine()) != null) {
		                response.append(line);
		            }
		        }
		    } 
		    catch(UnknownHostException un)
		    {
		    	DatingApplication.getService().print_toast("Something Went Wrong . Check Your Internet Connection");
		    }catch (Exception e) {
		        Log.e("demo", e.getMessage());
		    }
		    Log.d("demo", "Done with HTTP getting");
		    Log.e("response", response.toString());
		    try {
				JSONObject temp= new JSONObject(response.toString());
				sid =temp.getString("Result");
				Log.e("final sid is ", sid);
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		    return response.toString();
			}catch(Exception e)
			{
				
			}

			 return null;
		} 
		@Override
		protected void onPostExecute(String data) { 
			new SendHttpPostTask().execute();
			
			} 
		}
	
	private class SendHttpPostTask extends AsyncTask<String, Void, String> 
	{ 
		@Override 
		protected String doInBackground(String... params) 
		
		{ 
			try
			{
				String Url = "https://api.mOTP.in/v1/"+API_KEY+"/"+sid+"%20/%20-d%20"+PRIVATE_KEY;
			
			Log.e("post url is ", Url);
			
		    StringBuilder response = new StringBuilder();
		    try {
		        HttpPost post = new HttpPost();
		        post.setURI(new URI(Url));
		        
		        DefaultHttpClient httpClient = new DefaultHttpClient();
		        HttpResponse httpResponse = httpClient.execute(post);
		        if (httpResponse.getStatusLine().getStatusCode() == 200) {
		            Log.d("post", "HTTP POST succeeded");
		            HttpEntity messageEntity = httpResponse.getEntity();
		            InputStream is = messageEntity.getContent();
		            BufferedReader br = new BufferedReader(new InputStreamReader(is));
		            String line;
		            while ((line = br.readLine()) != null) {
		                response.append(line);
		            }
		        } else {
		            Log.e("post", "HTTP POST status code is not 200");
		        }
		    } catch (Exception e) {
		        Log.e("post", e.getMessage());
		    }
		    Log.d("post", "Done with HTTP posting");
		    Log.e("post resonse is ", response.toString());
		    return response.toString();
			}catch(Exception e)
			{
				
			}

			 return null;
		} 
		@Override
		protected void onPostExecute(String data) { 
			} 
		}
	
	private class GetWallMessages extends AsyncTask<String, Void, String> 
	{ 
		@Override 
		protected String doInBackground(String... params) 
		
		{ 
			try{
				String Url = "http://imaniacgroup.com/api/dating/getWallMessages.php?start=0&limit=20";
			
			//Log.e("get wall messages is ", Url);
			statuses=new ArrayList<WallMessage>();
		    StringBuilder response = new StringBuilder();
		    try {
		        HttpPost post = new HttpPost();
		        post.setURI(new URI(Url));
		        
		        DefaultHttpClient httpClient = new DefaultHttpClient();
		        HttpResponse httpResponse = httpClient.execute(post);
		        if (httpResponse.getStatusLine().getStatusCode() == 200) {
		            Log.d("post", "HTTP POST succeeded");
		            HttpEntity messageEntity = httpResponse.getEntity();
		            InputStream is = messageEntity.getContent();
		            BufferedReader br = new BufferedReader(new InputStreamReader(is));
		            String line;
		            while ((line = br.readLine()) != null) {
		                response.append(line);
		            }
		        } else {
		            Log.e("post", "HTTP POST status code is not 200");
		        }
		        
		    }catch(UnknownHostException un)
		    {
		    	DatingApplication.getService().print_toast("Something Went Wrong . Check Your Internet Connection");
		    } 
		    catch (Exception e) {
		        Log.e("post", e.getMessage());
		    }
		    Log.d("post", "Done with HTTP posting");
		   // Log.e("post resonse is ", response.toString());
		   // return response.toString();
		
		    JSONObject obj;
			try {
				obj = new JSONObject(response.toString());
				JSONArray array=obj.getJSONArray("__imaniac_api__");
				if(array.length()==0)
					default_message.setText("No Messages to Display");
				for(int i=0;i<array.length();i++)
				{
					
					//Log.e("element ", ""+array.getString(i));
					//uid=array.getString(i);
					try {
						JSONObject ob = array.getJSONObject(i);
						String uid=ob.getString("uid");
						String name=ob.getString("name");
						String msg=ob.getString("msg");
						String metro=ob.getString("metro");
					
						statuses.add(new WallMessage(uid, name, msg, metro));
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			
			
			}catch(Exception e)
			{
				
			}
			
			 return null;
		} 
		@Override
		protected void onPostExecute(String data) { 
			try{
				//Log.e("messages", ""+statuses.size() );
				
					setDefault();
			//	SampleAdapter adapter =new SampleAdapter(getActivity(), R.layout.layout_wall_messages,statuses);
			
					adapter.clear();
			adapter.addAll(statuses);
			if(statuses.size()>0)
				hide();
			adapter.notifyDataSetChanged();
					//listview.setAdapter(adapter);
			}catch(Exception e)
			{
				
			}
			} 
		}
	private class PostWallMessages extends AsyncTask<String, Void, String> 
	{ 
		@Override 
		protected String doInBackground(String... params) 
		
		{ 
			Log.e("post ", "executing");
			try
			{
			String Url = "http://imaniacgroup.com/api/dating/putWallStatus.php?uid="+DatingApplication.sp.getString("uid", "abc")+"&metro="+station.getSelectedItemPosition()+"&msg="+URLEncoder.encode(status.getText().toString());
			//status.setText("");
			Log.e("post wall url is ", Url);
			
		    StringBuilder response = new StringBuilder();
		    try {
		        HttpPost post = new HttpPost();
		        post.setURI(new URI(Url));
		        
		        DefaultHttpClient httpClient = new DefaultHttpClient();
		        HttpResponse httpResponse = httpClient.execute(post);
		        if (httpResponse.getStatusLine().getStatusCode() == 200) {
		            Log.d("post", "HTTP POST succeeded");
		            HttpEntity messageEntity = httpResponse.getEntity();
		            InputStream is = messageEntity.getContent();
		            BufferedReader br = new BufferedReader(new InputStreamReader(is));
		            String line;
		            while ((line = br.readLine()) != null) {
		                response.append(line);
		            }
		        } else {
		            Log.e("post", "HTTP POST status code is not 200");
		        }
		        
		    }catch(UnknownHostException un)
		    {
		    	DatingApplication.getService().print_toast("Something Went Wrong . Check Your Internet Connection");
		    }
		    catch (Exception e) {
		        Log.e("post", e.getMessage());
		    }
		    Log.d("post", "Done with HTTP posting");
		    Log.e("post resonse is ", response.toString());
		   // return response.toString();
		
		    /*JSONObject obj;
			try {
				obj = new JSONObject(response.toString());
				JSONArray array=obj.getJSONArray("__imaniac_api__");
				for(int i=0;i<array.length();i++)
				{
					
					//Log.e("element ", ""+array.getString(i));
					//uid=array.getString(i);
					try {
						JSONObject ob = array.getJSONObject(i);
						String uid=ob.getString("sender");
						String name=ob.getString("name");
						String msg=ob.getString("msg");
						String metro=ob.getString("metro");
						statuses.add(new WallMessage(uid, name, msg, metro));
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			*/
			}catch(Exception e)
			{
				Log.e("post wall exception", ""+e);
			}
			
			

			
			 return null;
		} 
		@Override
		protected void onPostExecute(String data) { 
			setEnable();
			new GetWallMessages().execute();
			} 
		}
	public void hide()
	{
		default_message.setVisibility(View.GONE);
	}
	public void setDefault()
	{
		if(statuses.size()==0)
		{
			
			no_connection.setVisibility(View.VISIBLE);
		}
		else
			no_connection.setVisibility(View.GONE);
	}
	public void setEnable()
	{
		post.setEnabled(true);
		progressGenerator.stop();
		status.setText("");
		post.setText("Post");
	}
	public class SampleAdapter extends ArrayAdapter<WallMessage> {
		List<WallMessage> mList;
		Context mContext;
		public SampleAdapter(Context context,int resource,List<WallMessage> list) {
			super(context,resource,list);
			mContext=context;
			mList=list;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				convertView = LayoutInflater.from(getContext()).inflate(R.layout.layout_wall_messages, null);
			}
			
			TextView title = (TextView) convertView.findViewById(R.id.textMain);
			title.setText(mList.get(position).getName());
			TextView msg = (TextView) convertView.findViewById(R.id.textMsg);
			msg.setText(mList.get(position).getMsg());
			TextView location=(TextView)convertView.findViewById(R.id.textLocation);
			try{
				location.setText(stations_list.get(Integer.parseInt(mList.get(position).getMetro())));
			}catch(Exception e)
			{}

			return convertView;
		}

	}
	
	
	public static void deleteLastCallLog(Context context, String phoneNumber) {
		
	    try {
	        //Thread.sleep(4000);
	    	//if(LoginActivity.sp.getString("status", "unregistered")!="registered")
	    		DatingApplication.getAct().setasregistered();
	        String strNumberOne[] = { phoneNumber };
	        Cursor cursor = context.getContentResolver().query(
	                CallLog.Calls.CONTENT_URI, null,
	                CallLog.Calls.NUMBER + " = ? ", strNumberOne, CallLog.Calls.DATE + " DESC");

	        if (cursor.moveToFirst()) {
	                int idOfRowToDelete = cursor.getInt(cursor.getColumnIndex(CallLog.Calls._ID));                      
	                int foo = context.getContentResolver().delete(
	                        CallLog.Calls.CONTENT_URI,
	                        CallLog.Calls._ID + " = ? ",
	                        new String[] { String.valueOf(idOfRowToDelete) });

	        }
	    } catch (Exception ex) {
	        Log.v("deleteNumber",
	                "Exception, unable to remove # from call log: "
	                        + ex.toString());
	    }
	}
	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onComplete(String status) {
		// TODO Auto-generated method stub
		
	}

}
