package com.imaniac.fragments;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;


import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import com.imaniac.application.DatingApplication;

import com.imaniac.databases.LocalStorageHandler;
import com.imaniac.dating.R;



import com.imaniac.service.OnlineService;
import com.imaniac.utils.ProgressGenerator;



import android.app.ProgressDialog;
import android.database.Cursor;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

public class ViewProfile extends Fragment implements com.imaniac.utils.ProgressGenerator.OnCompleteListener ,OnItemSelectedListener {
	public static final String EXTRAS_ENDLESS_MODE = "EXTRAS_ENDLESS_MODE";
	public static final String API_KEY = "0004-4a7d38c1-536b4a7b-3093-3a849e81";
	public static final String PRIVATE_KEY = "private=0009-4a7d38c1-536b4a7b-309c-50c9d751";
	public static final String DOMAIN = "imaniac.org";
	String sid;
	JSONObject obj1,temp;
	public static ProgressDialog pDialog;
	public static  String uid;
	ViewGroup current_view;
	ImageButton add_friend;
	//Scene current,next;
	ImageView iv;
	ProgressGenerator progressGenerator;
	//ActionProcessButton sign_up;
	TextView username,password,age,phone,email,address,gender,relationship;
	String Username,Password,Age,Phone,Email,Address,Gender,Relationship_Status;
	//int gender_pos,relation_pos;
	//Spinner gender,relationship;
	LinearLayout email_layout;
	String gender_list[] = {"male","female","others"};
	int sign_up_menu=0;
	boolean v_name=false,v_password=false,v_email = false,v_address=false,v_phone=false,v_age=false;
	String relationship_list[] = {"single","commited","married","divorcee"};
	
	
	private static LocalStorageHandler localstoragehandler; 
	private static Cursor dbCursor;
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		ViewGroup rootView=(ViewGroup)inflater.inflate(R.layout.layout_view_profile,container,false);
		
		current_view=(ViewGroup)rootView.findViewById(R.id.current_scene_layout);
		email_layout=(LinearLayout)rootView.findViewById(R.id.email_layout);
		Gender=gender_list[0];
		add_friend=(ImageButton)rootView.findViewById(R.id.btn_add_friend);
		Relationship_Status=relationship_list[0];
		iv=(ImageView)rootView.findViewById(R.id.profile_pic);
		username=(TextView)rootView.findViewById(R.id.name);
		//password=(FloatLabel)rootView.findViewById(R.id.id_password);
		age=(TextView)rootView.findViewById(R.id.age);
		phone=(TextView)rootView.findViewById(R.id.phone);
		email=(TextView)rootView.findViewById(R.id.email);
		address=(TextView)rootView.findViewById(R.id.address);
		gender=(TextView)rootView.findViewById(R.id.sex);
		relationship=(TextView)rootView.findViewById(R.id.relationship);
		//sign_up=(ActionProcessButton)rootView.findViewById(R.id.btnSignUp);
		//set_visibility();
		/*gender.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item,gender_list));
		relationship.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item,relationship_list));
		
		gender.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				Gender=gender_list[arg2];
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		relationship.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				Relationship_Status=relationship_list[arg2];
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		progressGenerator= new ProgressGenerator(this);
		
		sign_up.setMode(ActionProcessButton.Mode.ENDLESS);
		sign_up.setVisibility(View.GONE);
		sign_up.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(getActivity(), "clicked"+sign_up_menu, 1000).show();
				
					ConnectivityManager cm =
	        		        (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
	        		    NetworkInfo netInfo = cm.getActiveNetworkInfo();
	        		    if (netInfo != null && netInfo.isConnectedOrConnecting()) {
	        		    	Username=username.getEditText().getText().toString();
	    	            	Password=password.getEditText().getText().toString();
	    	            	Age=age.getEditText().getText().toString();
	    	            	Phone=phone.getEditText().getText().toString();
	    	            	Email=email.getEditText().getText().toString();
	    	            	Address=address.getEditText().getText().toString();
	    	            	Log.e("details", Username+Password+Age+Phone+Email+Address);
	    					validate();
	    					if(v_address&&v_age&&v_name&&v_email&&v_password)
	    						{
	    							progressGenerator.start(sign_up);
	    							Log.e("calllllled", "call");
	    							new CreateUserProfileTask().execute();
	    							//verify_phone_no();
	    						}
	    					else
	    						Log.e("notttttt", "called");
	        		    }
	        		    else
	        		    {
	        		    	Toast.makeText(getActivity(), "Not Connected to Internet", 2000).show();
	        		    }
				
					//progressGenerator.start(sign_up);
					//create_user_profile();
					
					
				
				
			}
		});
		*/
		
		localstoragehandler = new LocalStorageHandler(getActivity());
		dbCursor = localstoragehandler.get(OnlineService.friend_uid, DatingApplication.sp.getString("uid", "abc") );
		
		add_friend.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				new SendFriendRequest().execute(DatingApplication.view_profile_uid);
			}
		});
		new GetUserProfile().execute();
		return rootView;
	}
	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onComplete(String s) {
		// TODO Auto-generated method stub
		
	}
	public void setDetails()
	{
		Log.e("profile is ", DatingApplication.profile_uid);
		Log.e("view profile", DatingApplication.view_profile_uid);
		if(!DatingApplication.view_profile_uid.equalsIgnoreCase(DatingApplication.profile_uid))
		{
			phone.setVisibility(View.GONE);
			email_layout.setVisibility(View.GONE);
			add_friend.setVisibility(View.VISIBLE);
			Log.e("setting visibility", "visible");
		}
		else
		{
			phone.setVisibility(View.VISIBLE);
			email_layout.setVisibility(View.VISIBLE);
			add_friend.setVisibility(View.GONE);
			Log.e("setting visibility", "gone");
		}
		if(DatingApplication.approvedFriends.contains(DatingApplication.view_profile_uid))
		{
			Log.e("allready", "friend");
			phone.setVisibility(View.GONE);
			email_layout.setVisibility(View.GONE);
			add_friend.setVisibility(View.GONE);
		}
		if(DatingApplication.uids.contains(DatingApplication.view_profile_uid))
			Log.e("uid present", "uid");
		else
			Log.e("uid absent","uid");
		if(localstoragehandler.check_friend_request(DatingApplication.view_profile_uid)||DatingApplication.uids.contains(DatingApplication.view_profile_uid))
			add_friend.setVisibility(View.GONE);
		username.setText(Username);
		phone.setText(Phone);
		age.setText(Age);
		email.setText(Email);
		address.setText(Address);
		gender.setText(Gender);
		relationship.setText(Relationship_Status);
		
	}
	/*public void set_visibility()
	{
		password.setVisibility(View.GONE);
		username.getEditText().setFocusable(false);
		username.getEditText().setFocusableInTouchMode(false);
		username.getEditText().setClickable(false);
		phone.getEditText().setFocusable(false);
		phone.getEditText().setFocusableInTouchMode(false);
		phone.getEditText().setClickable(false);
		age.getEditText().setFocusable(false);
		age.getEditText().setFocusableInTouchMode(false);
		age.getEditText().setClickable(false);
		email.getEditText().setFocusable(false);
		email.getEditText().setFocusableInTouchMode(false);
		email.getEditText().setClickable(false);
		address.getEditText().setFocusable(false);
		address.getEditText().setFocusableInTouchMode(false);
		address.getEditText().setClickable(false);
		gender.setSelected(false);
		relationship.setSelected(false);
		relationship.setActivated(false);
		gender.setActivated(false);
		gender.setClickable(false);
		relationship.setClickable(false);
		
	}
	public void validate()
	{
		Log.e("validate function", "called");
		
		
		try{
			v_email= new EmailValidator(getActivity()).isValid(Email);
		}catch(Exception e)
		{
			
		}
		try{
			v_password= new NotEmptyValidator(getActivity()).isValid(Password);
		}catch(Exception e)
		{
			
		}
		try{
			v_name=new NotEmptyValidator(getActivity()).isValid(Password);
		}catch(Exception e)
		{
			
		}
		try{
			v_phone=new PhoneValidator(getActivity()).isValid(Phone);
		}catch(Exception e)
		{
			
		}
		try{
			v_address=new NotEmptyValidator(getActivity()).isValid(Address);
		}catch(Exception e)
		{
			
		}
		try{
			v_age = Integer.parseInt(Age)>18;
		}catch(Exception e)
		{
			
		}
		
		
		
		
		
		//Log.e("email validate", ""+v_email);
		if(!v_address)
			address.getEditText().setError("Address cannot be empty");
		if(!v_email)
			email.getEditText().setError("Email is not in correct format");
		if(!v_password)
			password.getEditText().setError("Password cannot be empty");
		
		if(!v_name)
			username.getEditText().setError("Name cannot be empty");
		if(!v_age)
			age.getEditText().setError("Age must be greater than 18");
		
	}
	
	private class CreateUserProfileTask extends AsyncTask<String, Void, String> 
	{

		

		

		@Override 
		protected String doInBackground(String... params) 
		
		{ 
			String U="http://imaniacgroup.com/api/dating/updateUserProfile.php?uid="+DatingApplication.sp.getString("uid", "abc")+"&name="+Username+"&pass="+Password+"&age="+Age+"&gender="+Gender+"&mstat="+Relationship_Status+"&phone="+Phone+"&email="+Email+"&address="+Address;
			Log.e("create string passing", U);
			HttpClient client=new DefaultHttpClient();
			HttpResponse response = null;
			URI url = null;
			try {
				url = new URI(URLEncoder.encode(U));
				HttpGet get=new HttpGet();
				get.setURI(url);
				try {
					response = client.execute(get);
				} catch (ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				HttpEntity entity=response.getEntity();
				try {
					String s=EntityUtils.toString(entity);
					Log.e("s is ",s);
					obj1 = new JSONObject(response.toString());
					temp= obj1.getJSONObject("__imaniac_api__");
					//Log.e("trial", obj1.getString("__imaniac_api__"));
					sid =temp.getString("status");
					Log.e("status received ", sid);
					uid = temp.getString("uid");
					Log.e("uid", uid);
					
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}catch (Exception e)
			{
				 Handler handler=new Handler();
				    handler=  new Handler(getActivity().getMainLooper());
				    handler.post( new Runnable(){
				        public void run(){
				        	if(sid!=null)
				        		Toast.makeText(getActivity(), sid,Toast.LENGTH_LONG).show();
							else
								try {
									if(obj1.getString("__imaniac_api__")!=null)
										Toast.makeText(getActivity(),obj1.getString("__imaniac_api__") ,Toast.LENGTH_LONG).show(); 
									else
										Toast.makeText(getActivity(), "Something Went Wrong ",Toast.LENGTH_LONG).show();
								} catch (JSONException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								} 
				        }
				    });
				//Toast.makeText(getActivity(), "Something Went Wrong ", 2000).show();
				//DatingApplication.getAct().switchContent(new LoginFragment());
			}
			
			
			
			
			
			
			
			

			 return null;
		} 
		@Override
		protected void onPostExecute(String data) { 
			//verify_phone_no();
		}
	}
	
	private class GetUserProfileTask extends AsyncTask<String, Void, String> 
	{

		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			pDialog=new ProgressDialog(getActivity());
			pDialog.setTitle("Please Wait");
			// Set progressbar message
			pDialog.setMessage("Verifying your Mobile No ");
			pDialog.setIndeterminate(false);
			//pDialog.setCancelable(false);

			// Show progressbar
			//pDialog.show();
		} 
		

		@Override 
		protected String doInBackground(String... params) 
		
		{ 
			String U="http://imaniacgroup.com/api/dating/getUserProfile.php?uid="+DatingApplication.sp.getString("uid", "abc");
			Log.e("create string passing", U);
			HttpClient client=new DefaultHttpClient();
			HttpResponse response = null;
			URI url = null;
			try {
				url = new URI(URLEncoder.encode(U));
				HttpGet get=new HttpGet();
				get.setURI(url);
				try {
					response = client.execute(get);
				} catch (ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				HttpEntity entity=response.getEntity();
				try {
					String s=EntityUtils.toString(entity);
					Log.e("s is ",s);
					obj1 = new JSONObject(s);
					JSONArray array = null;
					try {
						array = obj1.getJSONArray("__imaniac_api__");
						JSONObject ob = array.getJSONObject(0);
						Log.e("json is ", ""+ob.toString());
						username.getEditText().setText(ob.getString("name"));
						age.getEditText().setText(ob.getString("age"));
						phone.getEditText().setText(ob.getString("phone"));
						email.getEditText().setText(ob.getString("email"));
						address.getEditText().setText(ob.getString("address"));
							
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}catch (Exception e)
			{
				Log.e("getting information", "exception "+e.toString());
				
				//Toast.makeText(getActivity(), "Something Went Wrong ", 2000).show();
				//DatingApplication.getAct().switchContent(new LoginFragment());
			}
			
			
			
			
			
			
			
			

			 return null;
		} 
		@Override
		protected void onPostExecute(String data) { 
			//verify_phone_no();
		}
	}
	*/
	private class GetUserProfile extends AsyncTask<String, Void, String> 
	{ 
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pDialog=new ProgressDialog(getActivity());
			pDialog.setTitle("Please Wait");
			// Set progressbar message
			pDialog.setMessage("Accessing User Profile ");
			pDialog.setIndeterminate(false);
			//pDialog.setCancelable(false);
			pDialog.setCanceledOnTouchOutside(false);
			// Show progressbar
			pDialog.show();
			
		} 
		
		@Override 
		protected String doInBackground(String... params) 
		
		{ 
			try{
			//Log.e("verfiy", "dialog");
			 String Url="http://imaniacgroup.com/api/dating/getUserProfile.php?uid="+DatingApplication.view_profile_uid;
			
				URI url = null;
				try {
					url = new URI(Url);
				} catch (URISyntaxException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				HttpClient client=new DefaultHttpClient();
				
				HttpGet get=new HttpGet();
				get.setURI(url);
				
				HttpResponse response = null;
				try {
					response = client.execute(get);
				} catch (ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				HttpEntity entity=response.getEntity();
				String s = null;
				
					try {
						s=EntityUtils.toString(entity);
						Log.e("s ", s);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					JSONObject obj = null;
					try {
						obj = new JSONObject(s);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					JSONArray array = null;
					try {
						array = obj.getJSONArray("__imaniac_api__");
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						Log.e("exp", "called");
						e.printStackTrace();
					}
					
					

					for(int i=0;i<array.length();i++)
					{
						
						// Log.e("checking user profile", Url);
						try {
							JSONObject ob = array.getJSONObject(i);
							Log.e("ob is ", ob.toString());
							Username=ob.getString("name");
							Age=ob.getString("age");
							Phone=ob.getString("phone");
							Email=ob.getString("email");
							Address=ob.getString("address");
							Gender=ob.getString("gender");
							Relationship_Status	=ob.getString("mstat");
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							Log.e("json exception ", e.toString());
							e.printStackTrace();
						}
						
						
						
					}
					
					
					
					
			}catch(Exception e)
			{
DatingApplication.getAct().runOnUiThread(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						DatingApplication.getService().print_toast("Seems Internet Not Working.Unable to fetch profile Details.");
						DatingApplication.getAct().switchContent(new WallFragment());
					}
				});
				Log.e("Exction profile","called"+e.toString());
			}

			 return null;
		} 
		@Override
		protected void onPostExecute(String data) { 
			pDialog.hide();
			setDetails();
		}
	}
	private class SendFriendRequest extends AsyncTask<String, Void, String> 
	{ 
		@Override 
		protected String doInBackground(String... params) 
		
		{ 
			String Url = "http://imaniacgroup.com/api/dating/friendRequest.php?from="+DatingApplication.sp.getString("uid", "abc")+"&to="+params[0];
			Log.e("friend url is ", Url);
			
		    StringBuilder response = new StringBuilder();
		    try {
		        HttpPost post = new HttpPost();
		        post.setURI(new URI(Url));
		        
		        DefaultHttpClient httpClient = new DefaultHttpClient();
		        HttpResponse httpResponse = httpClient.execute(post);
		        if (httpResponse.getStatusLine().getStatusCode() == 200) {
		            Log.d("post", "HTTP POST succeeded");
		            HttpEntity messageEntity = httpResponse.getEntity();
		            InputStream is = messageEntity.getContent();
		            BufferedReader br = new BufferedReader(new InputStreamReader(is));
		            String line;
		            while ((line = br.readLine()) != null) {
		                response.append(line);
		            }
		        } else {
		            Log.e("post", "HTTP POST status code is not 200 but "+httpResponse.getStatusLine().getStatusCode());
		        }
		    } catch (Exception e) {
		        Log.e("post", e.getMessage());
		    }
		    Log.d("post", "Done with HTTP posting");
		    Log.e("post resonse is ", response.toString());
		    
		   
		   // return response.toString();
		

			 return null;
		} 
		@Override
		protected void onPostExecute(String data) { 
			 Handler handler=new Handler();
			    handler=  new Handler(getActivity().getMainLooper());
			    handler.post( new Runnable(){
			        public void run(){
			        	try
			        	{			
									Toast.makeText(getActivity(), "Friend Request Sent",Toast.LENGTH_LONG).show();
									try{
										add_friend.setVisibility(View.GONE);
									}catch(Exception e)
									{
										
									}
			        	} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} 
			        }
			    });
		}
	}

}
