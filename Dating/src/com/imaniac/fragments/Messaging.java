package com.imaniac.fragments;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.imaniac.application.DatingApplication;
import com.imaniac.databases.LocalStorageHandler;
import com.imaniac.dating.LoginActivity;
import com.imaniac.dating.MainActivity;
import com.imaniac.dating.R;
import com.imaniac.service.OnlineService;
import com.imaniac.types.FriendInfo;


public class Messaging extends Fragment{
	
	private static final int MESSAGE_CANNOT_BE_SENT = 0;
	public String username,msg;
	private EditText messageText;
	private static EditText messageHistoryText;
	private Button sendMessageButton;
	
	private FriendInfo friend ;
	private static LocalStorageHandler localstoragehandler; 
	private static Cursor dbCursor;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		ViewGroup rootView=(ViewGroup)inflater.inflate(R.layout.messaging_screen, container,false);
		messageHistoryText = (EditText)rootView.findViewById(R.id.messageHistory);
		
		messageText = (EditText) rootView.findViewById(R.id.message);
		
		messageText.requestFocus();			
		
		sendMessageButton = (Button) rootView.findViewById(R.id.sendMessageButton);
		
		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		
		localstoragehandler = new LocalStorageHandler(getActivity());
		dbCursor = localstoragehandler.get(OnlineService.friend_uid, DatingApplication.sp.getString("uid", "abc") );
		
		if (dbCursor.getCount() > 0){
		int noOfScorer = 0;
		dbCursor.moveToFirst();
		    while ((!dbCursor.isAfterLast())&&noOfScorer<dbCursor.getCount()) 
		    {
		        noOfScorer++;

				this.appendToMessageHistory(dbCursor.getString(2) , dbCursor.getString(3));
		        dbCursor.moveToNext();
		    }
		}
		localstoragehandler.close();
		
		if (msg != null) 
		{
			this.appendToMessageHistory(OnlineService.friend_uid , msg);
			
		}
		
		sendMessageButton.setOnClickListener(new OnClickListener(){
			CharSequence message;
			Handler handler = new Handler();
			public void onClick(View arg0) {
				message = messageText.getText();
				if (message.length()>0) 
				{		
					appendToMessageHistory(DatingApplication.sp.getString("uid", "abc"), message.toString());
					
					localstoragehandler.insert(DatingApplication.sp.getString("uid", "abc"),"Me", OnlineService.friend_uid,OnlineService.friend_name, message.toString());
							new SendMessage().execute(messageText.getText().toString());	
					messageText.setText("");
					
										
				}
				
			}});
		
		messageText.setOnKeyListener(new OnKeyListener(){
			public boolean onKey(View v, int keyCode, KeyEvent event) 
			{
				if (keyCode == 66){
					sendMessageButton.performClick();
					return true;
				}
				return false;
			}
			
			
		});
	}	
	
	
	

	@Override
	public void onPause() {
		super.onPause();
		
		//getActivity().unbindService(mConnection);
		
		//FriendController.setActiveFriend(null);
		
	}

	@Override
	public void onResume() 
	{		
		super.onResume();
		//bindService(new Intent(Messaging.this, IMService.class), mConnection , Context.BIND_AUTO_CREATE);
				
		//IntentFilter i = new IntentFilter();
	//	i.addAction(IMService.TAKE_MESSAGE);
		
		//getActivity().registerReceiver(messageReceiver, i);
		
		//FriendController.setActiveFriend(friend.userName);		
		
		
	}
	
	
	
	public  static void appendToMessageHistory(String username, String message) {
		if (username != null && message != null) {
			messageHistoryText.append(username + ":\n");								
			messageHistoryText.append(message + "\n");
		}
	}
	
	@Override
	public void onDestroy() {
	    super.onDestroy();
	    if (localstoragehandler != null) {
	    	localstoragehandler.close();
	    }
	    if (dbCursor != null) {
	    	dbCursor.close();
	    }
	    
	    
	}
	
	private class SendMessage extends AsyncTask<String, Void, String> 
	{ 
		@Override 
		protected String doInBackground(String... params) 
		
		{ 
			String Url = "http://imaniacgroup.com/api/dating/putMessage.php?sender="+DatingApplication.sp.getString("uid", "abc")+"&receiver="+OnlineService.friend_uid+"&msg="+URLEncoder.encode(params[0]);
					Log.e("post url is ", Url);
			
		    StringBuilder response = new StringBuilder();
		    try {
		        HttpPost post = new HttpPost();
		        post.setURI(new URI(Url));
		        
		        DefaultHttpClient httpClient = new DefaultHttpClient();
		        HttpResponse httpResponse = httpClient.execute(post);
		        if (httpResponse.getStatusLine().getStatusCode() == 200) {
		            Log.d("post", "HTTP POST succeeded");
		            HttpEntity messageEntity = httpResponse.getEntity();
		            InputStream is = messageEntity.getContent();
		            BufferedReader br = new BufferedReader(new InputStreamReader(is));
		            String line;
		            while ((line = br.readLine()) != null) {
		                response.append(line);
		            }
		        } else {
		            Log.e("post", "HTTP POST status code is not 200 but "+httpResponse.getStatusLine().getStatusCode());
		        }
		    } catch (Exception e) {
		        Log.e("post", e.getMessage());
		    }
		    Log.d("post", "Done with HTTP posting");
		    Log.e("post resonse is ", response.toString());
		    return response.toString();
		

			 //return null;
		} 
		@Override
		protected void onPostExecute(String data) { 
			} 
		}
	
	public static void update()
	{
		localstoragehandler = new LocalStorageHandler(OnlineService.mContext);
		dbCursor = localstoragehandler.get(OnlineService.friend_uid, DatingApplication.sp.getString("uid", "abc") );
		
		if (dbCursor.getCount() > 0){
		int noOfScorer = 0;
		dbCursor.moveToFirst();
		    while ((!dbCursor.isAfterLast())&&noOfScorer<dbCursor.getCount()) 
		    {
		        noOfScorer++;

				appendToMessageHistory(dbCursor.getString(2) , dbCursor.getString(3));
		        dbCursor.moveToNext();
		    }
		}
		localstoragehandler.close();
	}

}
