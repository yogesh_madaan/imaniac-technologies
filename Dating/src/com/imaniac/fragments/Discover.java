package com.imaniac.fragments;




import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.imaniac.api.RegisteredUser;
import com.imaniac.application.DatingApplication;
import com.imaniac.dating.MainActivity;
import com.imaniac.dating.R;
import com.imaniac.types.UserInfo;

public class Discover extends Fragment implements OnItemClickListener{
	private GridView gridview;
	SampleAdapter adapter;
	List<UserInfo> ll = new ArrayList<UserInfo>();
	public static int mPosition;
	LinearLayout rl;
	TextView default_message;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		ViewGroup rootView=(ViewGroup)inflater.inflate(R.layout.layout_discover, container,false);
		gridview = (GridView)rootView.findViewById(R.id.gridView1);
		rl=(LinearLayout)rootView.findViewById(R.id.layout_main);
		/*String[] countries = getResources().getStringArray(R.array.countries_array);
		ArrayAdapter<String> aa = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, countries);
		gridview.setAdapter(aa);
		*/
		gridview.setOnItemClickListener(this);
		default_message = (TextView)rootView.findViewById(R.id.textView1);
		return rootView;
	}
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		adapter= new SampleAdapter(getActivity());
		new SetFriends().execute();
		MainActivity.active_fragment=0;
		
	}
	
	

	public class SampleAdapter extends ArrayAdapter<UserInfo> {

		public SampleAdapter(Context context) {
			super(context, 0);
		}

		public View getView(final int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				convertView = LayoutInflater.from(getContext()).inflate(R.layout.layout_discover_item, null);
			}
			LinearLayout rl =(LinearLayout)convertView.findViewById(R.id.main_layout);
			rl.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					DatingApplication.view_profile_uid=ll.get(position).getUid();
					DatingApplication.getAct().switchContent(new ViewProfile());
				}
			});
			TextView name = (TextView)convertView.findViewById(R.id.name);
			name.setText(ll.get(position).getName());
			TextView details = (TextView)convertView.findViewById(R.id.details);
			details.setText("Age: "+ll.get(position).getAge());
			ImageButton add=(ImageButton)convertView.findViewById(R.id.add_friend);
			add.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					new SendFriendRequest().execute(ll.get(position).getUid());
				}
			});
			mPosition=position;
			
			return convertView;
		}

	}
	
	private class SetFriends extends AsyncTask<String, Void, String> 
	{ 
		@Override 
		protected String doInBackground(String... params) 
		
		{ 
			
			RegisteredUser ru=new RegisteredUser();
			try {
				ll=ru.search("abc");
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		   // return response.toString();
		

			 return null;
		} 
		@Override
		protected void onPostExecute(String data) { 
			
			if(ll.size()==0)
				{
					setDefault();
					hide();
					
				}
			else{
				
				hide();
				for (int i = 0; i<ll.size(); i++) {
					adapter.add(new UserInfo(ll.get(i).getName(), ll.get(i).getUid(), ll.get(i).getAge()));
				}
				gridview.setAdapter(adapter);
			}
		        
		}
	}
	public void hide()
	{
		default_message.setVisibility(View.GONE);
	}
	public void setDefault()
	{
		rl.setBackgroundResource(R.drawable.deafult_no_connection);
	}
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		DatingApplication.view_profile_uid=ll.get(arg2).getUid();
		DatingApplication.getAct().switchContent(new ViewProfile());
	/*	if(DatingApplication.sp.getString("reg_status", "unregistered").equalsIgnoreCase("registered"))
			new SendFriendRequest().execute(ll.get(arg2).getUid());
		else
			Toast.makeText(getActivity(), "You Must Register First", 2000).show();*/
	}
	
	private class SendFriendRequest extends AsyncTask<String, Void, String> 
	{ 
		@Override 
		protected String doInBackground(String... params) 
		
		{ 
			String Url = "http://imaniacgroup.com/api/dating/friendRequest.php?from="+DatingApplication.sp.getString("uid", "abc")+"&to="+params[0];
			Log.e("friend url is ", Url);
			
		    StringBuilder response = new StringBuilder();
		    try {
		        HttpPost post = new HttpPost();
		        post.setURI(new URI(Url));
		        
		        DefaultHttpClient httpClient = new DefaultHttpClient();
		        HttpResponse httpResponse = httpClient.execute(post);
		        if (httpResponse.getStatusLine().getStatusCode() == 200) {
		            Log.d("post", "HTTP POST succeeded");
		            HttpEntity messageEntity = httpResponse.getEntity();
		            InputStream is = messageEntity.getContent();
		            BufferedReader br = new BufferedReader(new InputStreamReader(is));
		            String line;
		            while ((line = br.readLine()) != null) {
		                response.append(line);
		            }
		        } else {
		            Log.e("post", "HTTP POST status code is not 200 but "+httpResponse.getStatusLine().getStatusCode());
		        }
		    } catch (Exception e) {
		        Log.e("post", e.getMessage());
		    }
		    Log.d("post", "Done with HTTP posting");
		    Log.e("post resonse is ", response.toString());
		    
		   
		   // return response.toString();
		

			 return null;
		} 
		@Override
		protected void onPostExecute(String data) { 
			 Handler handler=new Handler();
			    handler=  new Handler(getActivity().getMainLooper());
			    handler.post( new Runnable(){
			        public void run(){
			        	try
			        	{			
									Toast.makeText(getActivity(), "Friend Request Sent",Toast.LENGTH_LONG).show();
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} 
			        }
			    });
		}
	}
	
	
	    
	    
	    
	    
	    
	    

	 


}
