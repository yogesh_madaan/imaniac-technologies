package com.imaniac.fragments;

import java.util.ArrayList;
import java.util.List;

import com.imaniac.application.DatingApplication;
import com.imaniac.databases.LocalStorageHandler;
import com.imaniac.dating.R;
import com.imaniac.fragments.SampleListFragment2.SampleAdapter;
import com.imaniac.fragments.SampleListFragment2.SampleItem;
import com.imaniac.service.OnlineService;
import com.imaniac.types.WallMessage;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class ChatFragment extends Fragment{
	ListView listview;
	private LocalStorageHandler localstoragehandler; 
	private Cursor dbCursor;
	List<String> friend_uid,friend_name;
	TextView default_message;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		ViewGroup rootView=(ViewGroup)inflater.inflate(R.layout.layout_chat_wall, container,false);
		listview = (ListView)rootView.findViewById(R.id.listview);
		default_message = (TextView)rootView.findViewById(R.id.textView1);
		friend_name=new ArrayList<String>();
		friend_uid=new ArrayList<String>();
		return rootView;
	}
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		localstoragehandler = new LocalStorageHandler(getActivity());
		dbCursor = localstoragehandler.get_friends();
		Log.e("chat list count", ""+dbCursor.getCount());
		dbCursor.moveToFirst();
		int noOfScorer = 0;
	    while ((!dbCursor.isAfterLast())&&noOfScorer<dbCursor.getCount()) 
	    {
	    	Log.e("while", "entered");
	        noOfScorer++;

			friend_name.add(dbCursor.getString(1));
			friend_uid.add(dbCursor.getString(2));
	        dbCursor.moveToNext();
	    }
	    if(OnlineService.noti_count==1)
	    {
		    	try{
		    
		    	OnlineService.friend_name=friend_name.get(dbCursor.getCount());
				OnlineService.friend_uid=friend_uid.get(dbCursor.getCount());
				DatingApplication.getService().startMessaging();
		    }catch(Exception e)
		    {
		    	Log.e("jump", "exception");
		    }
	}
	    
	    SampleAdapter adapter = new SampleAdapter(getActivity());;
	    for (int i = 0; i<friend_name.size(); i++) {
			adapter.add(new SampleItem(friend_name.get(i), android.R.drawable.ic_menu_search));
		if(friend_name.size()>0)
			default_message.setVisibility(View.GONE);
	    listview.setAdapter(adapter);
	    //listview.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1,friend_name));
	    listview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				OnlineService.friend_name=friend_name.get(arg2);
				OnlineService.friend_uid=friend_uid.get(arg2);
				DatingApplication.getService().startMessaging();
				//DatingApplication.getAct().switchContent(new MessagingA);
			}
		});
		
	}
	}
	public class SampleItem {
		public String tag;
		public int iconRes;
		public SampleItem(String tag, int iconRes) {
			this.tag = tag; 
			this.iconRes = iconRes;
		}
	}
	public class SampleAdapter extends ArrayAdapter<SampleItem> {
		
		Context mContext;
		
		public SampleAdapter(Context context) {
			super(context,0);
			mContext=context;
			
			
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				convertView = LayoutInflater.from(getContext()).inflate(R.layout.layout_chat_messages, null);
			}
			
			TextView title = (TextView) convertView.findViewById(R.id.textMain);
			title.setText(friend_name.get(position));
			Log.e("friend name is ", ""+friend_name.get(position));
			TextView message_counter=(TextView)convertView.findViewById(R.id.message_counter);
			if(localstoragehandler.check_unread_messages_specific(friend_uid.get(position))>0)
			{
				message_counter.setText(""+localstoragehandler.check_unread_messages_specific(friend_uid.get(position)));
				message_counter.setVisibility(View.VISIBLE);
			}

			return convertView;
		}

	}

}

