package com.imaniac.fragments;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.CallLog;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnScrollChangedListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Toast;

import com.dd.processbutton.iml.ActionProcessButton;
import com.iangclifton.android.floatlabel.FloatLabel;
import com.imaniac.application.DatingApplication;
import com.imaniac.dating.LoginActivity;
import com.imaniac.dating.MainActivity;
import com.imaniac.dating.R;
import com.imaniac.service.GPSTracker;
import com.imaniac.utils.ProgressGenerator;
import com.throrinstudio.android.common.libs.validator.validator.EmailValidator;
import com.throrinstudio.android.common.libs.validator.validator.NotEmptyValidator;
import com.throrinstudio.android.common.libs.validator.validator.PhoneValidator;

public class LoginFragment extends Fragment implements com.imaniac.utils.ProgressGenerator.OnCompleteListener ,OnItemSelectedListener{
	public static final String EXTRAS_ENDLESS_MODE = "EXTRAS_ENDLESS_MODE";
	public static final String API_KEY = "0004-4a7d38c1-536b4a7b-3093-3a849e81";
	public static final String PRIVATE_KEY = "private=0009-4a7d38c1-536b4a7b-309c-50c9d751";
	public static final String DOMAIN = "imaniac.org";
	String sid;
	JSONObject obj1,temp;
	public static ProgressDialog pDialog;
	public static  String uid;
	ViewGroup current_view;
	//Scene current,next;\
	LinearLayout blank;
	ImageView iv;
	ProgressGenerator progressGenerator;
	static ActionProcessButton btnSignIn,sign_up;
	FloatLabel username,password,age,phone,email,address,repassword;
	String Username,Password,Age,Phone,Email,Address,Gender,Relationship_Status;
	Spinner gender,relationship;
	String gender_list[] = {"male","female","others"};
	int sign_up_menu=0;
	boolean v_name=false,v_password=false,v_email = false,v_address=false,v_phone=false,v_age=false;
	String relationship_list[] = {"single","committed","married","divorcee"};
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		final ViewGroup rootView=(ViewGroup)inflater.inflate(R.layout.layout_login, container,false);
		current_view=(ViewGroup)rootView.findViewById(R.id.current_scene_layout);
		
		Gender=gender_list[0];
		Relationship_Status=relationship_list[0];
		iv=(ImageView)rootView.findViewById(R.id.profile_pic);
		username=(FloatLabel)rootView.findViewById(R.id.username);
		password=(FloatLabel)rootView.findViewById(R.id.password);
		repassword=(FloatLabel)rootView.findViewById(R.id.repeat_password);
		age=(FloatLabel)rootView.findViewById(R.id.age);
		phone=(FloatLabel)rootView.findViewById(R.id.phone);
		email=(FloatLabel)rootView.findViewById(R.id.email);
		address=(FloatLabel)rootView.findViewById(R.id.address);
		gender=(Spinner)rootView.findViewById(R.id.gender);
		relationship=(Spinner)rootView.findViewById(R.id.relationship_status);
		blank=(LinearLayout)rootView.findViewById(R.id.blank);
		sign_up=(ActionProcessButton)rootView.findViewById(R.id.btnSignUp);
		gender.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item,gender_list));
		relationship.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item,relationship_list));
		GPSTracker gpsTracker = new GPSTracker(getActivity());
		if (gpsTracker.canGetLocation())
        {
            String latitude = String.valueOf(gpsTracker.latitude);
           
            String longitude = String.valueOf(gpsTracker.longitude);
            DatingApplication.e.putString("latitude", latitude);
            DatingApplication.e.putString("longitude", longitude);
            DatingApplication.e.commit();
            Log.e("lat "+latitude, "long "+longitude);
            Log.e("postal", ""+gpsTracker.getAddressLine(getActivity()));
        }
        else
        {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gpsTracker.showSettingsAlert();
        }
		
		gender.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				Gender=gender_list[arg2];
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		relationship.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				Relationship_Status=relationship_list[arg2];
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		progressGenerator= new ProgressGenerator(this);
		btnSignIn = (ActionProcessButton)rootView.findViewById(R.id.btnSignIn);
		btnSignIn.setMode(ActionProcessButton.Mode.ENDLESS);
		sign_up.setMode(ActionProcessButton.Mode.ENDLESS);
		btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            	
        		ConnectivityManager cm =
        		        (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        		    NetworkInfo netInfo = cm.getActiveNetworkInfo();
        		    if (netInfo != null && netInfo.isConnectedOrConnecting()) {
        		    	//Toast.makeText(getActivity(), "clicked", 1000).show();
                    	//Username=username.getEditText().toString();
                    	Password=password.getEditText().getText().toString();
                    	//Age=age.getEditText().toString();
                    	Phone=phone.getEditText().getText().toString();
                    	//Email=email.getEditText().toString();
                    	//Address=address.getEditText().toString();
                    	//validate();
                    	Log.e("pass is ", Password);
                    	Log.e("no is ", Phone);
                        progressGenerator.start(btnSignIn);
                        btnSignIn.setEnabled(false);
                        //btnSignIn.setEnabled(false);
                        Timer timer=new Timer();
                        timer.schedule(new TimerTask() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								
			        			/*DatingApplication.getLoginAct().runOnUiThread( new Runnable() {
									public void run() {
										btnSignIn.setText("Sign In");
									}
								});*/
			        			
									try{
										progressGenerator.stop();
					        			btnSignIn.setEnabled(true);
										Toast.makeText(getActivity(), "Something Went Wrong ",Toast.LENGTH_LONG).show();
									}catch(Exception e)
									{
										
									}
								new SignInTask().cancel(true);
							}
						}, 30000);
                        new SignInTask().execute();
        		    }
        		    else
        		    {
        		    	Toast.makeText(getActivity(), "Not Connected to Internet", 2000).show();
        		    }
        		    
            	
               
            }
        });
		sign_up.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//Toast.makeText(getActivity(), "clicked"+sign_up_menu, 1000).show();
				rootView.clearFocus();
				if(sign_up_menu>0)
				{
					ConnectivityManager cm =
	        		        (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
	        		    NetworkInfo netInfo = cm.getActiveNetworkInfo();
	        		    if (netInfo != null && netInfo.isConnectedOrConnecting()) {
	        		    	Username=username.getEditText().getText().toString();
	    	            	Password=password.getEditText().getText().toString();
	    	            	Age=age.getEditText().getText().toString();
	    	            	Phone=phone.getEditText().getText().toString();
	    	            	Email=email.getEditText().getText().toString();
	    	            	Address=address.getEditText().getText().toString();
	    	            	Log.e("details", Username+Password+Age+Phone+Email+Address);
	    					validate();
	    					if(v_address&&v_age&&v_name&&v_email&&v_password&&Password.equals(repassword.getEditText().getText().toString()))
	    						{
	    							progressGenerator.start(sign_up);
	    							Log.e("calllllled", "call");
	    							 Timer timer=new Timer();
	    		                        timer.schedule(new TimerTask() {
	    									
	    									@Override
	    									public void run() {
	    										// TODO Auto-generated method stub
	    										progressGenerator.stop();
	    					        			sign_up.setEnabled(true);
	    					        			DatingApplication.getLoginAct().runOnUiThread( new Runnable() {
	    											public void run() {
	    												sign_up.setText("Sign Up");
	    											}
	    										});
	    					        			
	    											Toast.makeText(getActivity(), "Something Went Wrong ",Toast.LENGTH_LONG).show();
	    										new  CreateUserProfileTask().cancel(true);
	    									}
	    								}, 30000);
	    							create_user_profile();
	    							//verify_phone_no();
	    						}
	    					else
	    						{
	    							Log.e("notttttt", "called");
	    							phone.getEditText().requestFocus();
	    							sign_up.setEnabled(true);
	    						}
	        		    }
	        		    else
	        		    {
	        		    	Toast.makeText(getActivity(), "Not Connected to Internet", 2000).show();
	        		    }
				
					//progressGenerator.start(sign_up);
					//create_user_profile();
					
					
				}
				else
				{
					setVisibility();
					
				}
				
			}
			
		});
		/*sign_up.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
				
				/*if(sign_up_menu)
				{
					validate();
	               // progressGenerator.start(sign_up);
				}
				else
				{
					setVisibility();
					
				}
			}
		});*/
		setVisibility();
		/*rootView.setOnKeyListener(new OnKeyListener() {
			
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				// TODO Auto-generated method stub
				 if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
				        blank.setFocusable(true);
				        blank.setFocusableInTouchMode(true);
				        rootView.clearFocus();
				        return false;
				    }
				return false;
			}
		});
		ScrollView sc=(ScrollView)rootView.findViewById(R.id.scrollView1);
		sc.getViewTreeObserver().addOnScrollChangedListener(new OnScrollChangedListener() {
			
			@Override
			public void onScrollChanged() {
				// TODO Auto-generated method stub
				rootView.clearFocus();
			}
		});*/
		return rootView;
	}
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}
	@Override
	public void onComplete(String s) {
		// TODO Auto-generated method stub
		btnSignIn.setCompleteText(s);
		//Toast.makeText(getActivity(), "Loading_Complete", Toast.LENGTH_LONG).show();
	}
	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
		
	}
	public void setVisibility()
	{
		/*slideToTop(getView());
		Animation outAnimation;
         

        outAnimation = (Animation) AnimationUtils.loadAnimation (getActivity(), R.anim.slide_in_left);

        age.setAnimation(outAnimation);
        username.setAnimation(outAnimation);
        email.setAnimation(outAnimation);
        address.setAnimation(outAnimation);
        gender.setAnimation(outAnimation);
        relationship.setAnimation(outAnimation);
        outAnimation.setAnimationListener(new  AnimationListener() {

        @Override
        public void onAnimationEnd(Animation arg0) {
        	age.setVisibility(View.VISIBLE);
    		username.setVisibility(View.VISIBLE);
    		email.setVisibility(View.VISIBLE);
    		address.setVisibility(View.VISIBLE);
    		gender.setVisibility(View.VISIBLE);
    		relationship.setVisibility(View.VISIBLE);
    		btnSignIn.setVisibility(View.GONE);                          
        }
        @Override
        public void onAnimationRepeat(Animation animation) {
            // TODO Auto-generated method stub                          
        }

        @Override
        public void onAnimationStart(Animation animation) {
            // TODO Auto-generated method stub
        }                 
            });
    age.startAnimation(outAnimation);
    username.startAnimation(outAnimation);
    email.startAnimation(outAnimation);
    address.startAnimation(outAnimation);
    gender.startAnimation(outAnimation);
    relationship.startAnimation(outAnimation);*/
		if(LoginActivity.opt_selected==0)
		{
			sign_up.setVisibility(View.GONE);
		}
		else
		{
			
		
			iv.setVisibility(View.GONE);
			age.setVisibility(View.VISIBLE);
			username.setVisibility(View.VISIBLE);
			email.setVisibility(View.VISIBLE);
			address.setVisibility(View.VISIBLE);
			gender.setVisibility(View.VISIBLE);
			relationship.setVisibility(View.VISIBLE);
			repassword.setVisibility(View.VISIBLE);
			btnSignIn.setVisibility(View.GONE);        
			sign_up_menu=1;
		}
	}
	public void slideToTop(View view){
		
		
		/*TranslateAnimation animate = new TranslateAnimation(0,0,0,0);
		animate.setDuration(500);
		animate.setFillAfter(true);
		view.startAnimation(animate);
		view.setVisibility(View.VISIBLE);*/
		
		}
public void validate()
{
	Log.e("validate function", "called");
	
	if(!Password.equals(repassword.getEditText().getText().toString()))
		repassword.getEditText().setError("Password didn't match.");
	try{
		v_email= new EmailValidator(getActivity()).isValid(Email);
	}catch(Exception e)
	{
		
	}
	try{
		v_password= new NotEmptyValidator(getActivity()).isValid(Password);
	}catch(Exception e)
	{
		
	}
	try{
		v_name=new NotEmptyValidator(getActivity()).isValid(Password);
	}catch(Exception e)
	{
		
	}
	try{
		v_phone=new PhoneValidator(getActivity()).isValid(Phone);
	}catch(Exception e)
	{
		
	}
	try{
		v_address=new NotEmptyValidator(getActivity()).isValid(Address);
	}catch(Exception e)
	{
		
	}
	try{
		v_age = Integer.parseInt(Age)>18;
	}catch(Exception e)
	{
		
	}
	
	
	
	
	
	//Log.e("email validate", ""+v_email);
	if(!v_address)
		address.getEditText().setError("Address cannot be empty");
	if(!v_email)
		email.getEditText().setError("Email is not in correct format");
	if(!v_password)
		password.getEditText().setError("Password cannot be empty");
	/*if(!v_phone)
		address.getEditText().setError("Phone no is not in right format");*/
	if(!v_name)
		username.getEditText().setError("Name cannot be empty");
	if(!v_age)
		age.getEditText().setError("Age must be greater than 18");
	
}
public void create_user_profile()
{
	new CreateUserProfileTask().execute();
}
public void verify_phone_no()
{
	new SendHttpRequestTask().execute();
}

private class CreateUserProfileTask extends AsyncTask<String, Void, String> 
{

	

	

	@Override 
	protected String doInBackground(String... params) 
	
	{ 
		String U="http://imaniacgroup.com/api/dating/createUserProfile.php?name="+URLEncoder.encode(Username)+"&pass="+URLEncoder.encode(Password)+"&age="+URLEncoder.encode(Age)+"&gender="+URLEncoder.encode(Gender)+"&mstat="+URLEncoder.encode(Relationship_Status)+"&phone="+URLEncoder.encode(Phone)+"&email="+URLEncoder.encode(Email)+"&address="+URLEncoder.encode(Address);
		Log.e("create string passing", URLEncoder.encode(U));
		HttpClient client=new DefaultHttpClient();
		HttpResponse response = null;
		URI url = null;
		try {
			url = new URI(U);
			HttpGet get=new HttpGet();
			get.setURI(url);
			try {
				response = client.execute(get);
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			HttpEntity entity=response.getEntity();
			try {
				String s=EntityUtils.toString(entity);
				Log.e("s is ",s);
				obj1 = new JSONObject(s);
				temp= obj1.getJSONObject("__imaniac_api__");
				//Log.e("trial", obj1.getString("__imaniac_api__"));
				//sid =temp.getString("status");
				//Log.e("status received ", sid);
				uid = temp.getString("uid");
				Log.e("uid", uid);
				//DatingApplication.getService().restart();
				DatingApplication.getLoginAct().runOnUiThread(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						Toast.makeText(getActivity(), "Sign Up Successful", 1000).show();
						//getActivity().finish();
						GPSTracker gpsTracker = new GPSTracker(getActivity());
						if (gpsTracker.canGetLocation())
				        {
				            String latitude = String.valueOf(gpsTracker.latitude);
				           
				            String longitude = String.valueOf(gpsTracker.longitude);
				            DatingApplication.e.putString("latitide", latitude);
				            DatingApplication.e.putString("longitude", longitude);
				            DatingApplication.e.commit();
				            Log.e("lat "+latitude, "long "+longitude);
				        }
				        else
				        {
				            // can't get location
				            // GPS or Network is not enabled
				            // Ask user to enable GPS/network in settings
				            gpsTracker.showSettingsAlert();
				        }
						DatingApplication.e.putString("uid", uid);
						DatingApplication.e.putString("phone", Phone);
						DatingApplication.e.commit();
						Intent i = new Intent(getActivity(),MainActivity.class);
						MainActivity.verification_time=0;
						startActivity(i);
						getActivity().finish();
						//startActivity(new Intent(getActivity(),LoginActivity.class));
					}
				});
				
				
			}catch (Exception e)
			{
				Log.e("create user exception", ""+e.toString());
				DatingApplication.getLoginAct().runOnUiThread(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						//if(sid!=null)
			        		//Toast.makeText(getActivity(), sid,Toast.LENGTH_LONG).show();
						//else
							try {
								if(obj1.getString("__imaniac_api__")!=null)
									{
									progressGenerator.stop();
				        			sign_up.setEnabled(true);
				        			DatingApplication.getLoginAct().runOnUiThread( new Runnable() {
										public void run() {
											sign_up.setText("Sign Up");
										}
									});
										Toast.makeText(getActivity(),obj1.getString("__imaniac_api__") ,Toast.LENGTH_LONG).show(); 
									}
								else
									{
									progressGenerator.stop();
				        			sign_up.setEnabled(true);
				        			DatingApplication.getLoginAct().runOnUiThread( new Runnable() {
										public void run() {
											sign_up.setText("Sign Up");
										}
									});
										Toast.makeText(getActivity(), "Something Went Wrong ",Toast.LENGTH_LONG).show();
									}
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch(Exception e)
							{
								progressGenerator.stop();
			        			sign_up.setEnabled(true);
			        			DatingApplication.getLoginAct().runOnUiThread( new Runnable() {
									public void run() {
										sign_up.setText("Sign Up");
									}
								});
								Toast.makeText(getActivity(), "Something Went Wrong ",Toast.LENGTH_LONG).show();
							}
					}
				});
				
				
			}
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		
		
		
		

		 return null;
	} 
	@Override
	protected void onPostExecute(String data) { 
		//verify_phone_no();
	}
}
private class SendHttpRequestTask extends AsyncTask<String, Void, String> 
{ 
	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		pDialog=new ProgressDialog(getActivity());
		pDialog.setTitle("Please Wait");
		// Set progressbar message
		pDialog.setMessage("Verifying your Mobile No ");
		pDialog.setIndeterminate(false);
		//pDialog.setCancelable(false);

		// Show progressbar
		pDialog.show();
	} 
	
	@Override 
	protected String doInBackground(String... params) 
	
	{ 
		String Url = "https://api.mOTP.in/v1/"+API_KEY+"/91"+"9582854011";
		Log.e("url is ", Url);
		StringBuilder response = new StringBuilder();
	    try {
	        HttpGet get = new HttpGet();
	        get.setURI(new URI(Url));
	        DefaultHttpClient httpClient = new DefaultHttpClient();
	        HttpResponse httpResponse = httpClient.execute(get);
	        if (httpResponse.getStatusLine().getStatusCode() == 200) {
	            Log.d("demo", "HTTP Get succeeded");

	            HttpEntity messageEntity = httpResponse.getEntity();
	            InputStream is = messageEntity.getContent();
	            BufferedReader br = new BufferedReader(new InputStreamReader(is));
	            String line;
	            while ((line = br.readLine()) != null) {
	                response.append(line);
	            }
	        }
	    } catch (Exception e) {
	        Log.e("demo", e.getMessage());
	    }
	    Log.d("demo", "Done with HTTP getting");
	    Log.e("response", response.toString());
	    try {
			JSONObject temp= new JSONObject(response.toString());
			sid =temp.getString("Result");
			Log.e("final sid is ", sid);
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    return response.toString();
	

		 //return null;
	} 
	@Override
	protected void onPostExecute(String data) { 
		new SendHttpPostTask().execute();
		
		} 
	}
private class SignInTask extends AsyncTask<String, Void, String> 
{ 
	@Override 
	protected String doInBackground(String... params) 
	
	{ 
		String Url = "http://imaniacgroup.com/api/dating/authenticate.php?phone="+Phone+"&pass="+Password;
		Log.e("sign url is ", Url);
		
	    StringBuilder response = new StringBuilder();
	    try {
	        HttpPost post = new HttpPost();
	        post.setURI(new URI(Url));
	        
	        DefaultHttpClient httpClient = new DefaultHttpClient();
	        HttpResponse httpResponse = httpClient.execute(post);
	        if (httpResponse.getStatusLine().getStatusCode() == 200) {
	            Log.d("post", "HTTP POST succeeded");
	            HttpEntity messageEntity = httpResponse.getEntity();
	            InputStream is = messageEntity.getContent();
	            BufferedReader br = new BufferedReader(new InputStreamReader(is));
	            String line;
	            while ((line = br.readLine()) != null) {
	                response.append(line);
	            }
	        } else {
	            Log.e("post", "HTTP POST status code is not 200 but "+httpResponse.getStatusLine().getStatusCode());
	        }
	    } catch (Exception e) {
	        Log.e("post", e.getMessage());
	    }
	    Log.d("post", "Done with HTTP posting");
	    Log.e("post resonse is ", response.toString());
	    
	    try {
	    	obj1 = new JSONObject(response.toString());
			/*JSONArray arr = obj1.getJSONArray("imaniac_api");
			Log.e("result size is ", ""+arr.length());
			for(int i=0;i<arr.length();i++)
			{
				JSONObject temp  =arr.getJSONObject(i);
				
				sid =temp.getString("status");
				Log.e("status received ", sid);
				uid = temp.getString("uid");
				//Log.e("added", ""+urls.size());
			}*/
			temp= obj1.getJSONObject("__imaniac_api__");
			sid =temp.getString("status");
			Log.e("status received ", sid);
			uid = temp.getString("uid");
			/*if(sid=="success")
			{
				MainActivity.e.putString("uid", uid);
				MainActivity.e.commit();
				Intent i = new Intent(getActivity(),MainActivity.class);
				startActivity(i);
				getActivity().finish();
			}*/
			
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			
			try {
				//sid=obj1.getString("__imaniac_api__");
				Log.e("sid value", "changed" +sid);
			}catch(Exception e1)
			{
				Log.e("exception exception", "called" +e1);
			}
			e.printStackTrace();
		}
	    
	   // return response.toString();
	

		 return null;
	} 
	@Override
	protected void onPostExecute(String data) { 
		try{
			
			if(sid.equalsIgnoreCase("success"))
		
		{
				
				DatingApplication.e.putString("uid", uid);
				DatingApplication.e.putString("phone", Phone);
				DatingApplication.e.commit();
				Intent i = new Intent(getActivity(),MainActivity.class);
				MainActivity.verification_time=0;
				startActivity(i);
				getActivity().finish();
			//startActivity(new Intent(getActivity(),Login.class));
		}
			if(sid.equalsIgnoreCase("failed"))
			{
				 Handler handler=new Handler();
				    handler=  new Handler(getActivity().getMainLooper());
				    handler.post( new Runnable(){
				        public void run(){
				        			progressGenerator.stop();
				        			btnSignIn.setEnabled(true);
				        			DatingApplication.getLoginAct().runOnUiThread( new Runnable() {
										public void run() {
											btnSignIn.setText("Sign In");
										}
									});
									Toast.makeText(getActivity(), "Login Failed ",Toast.LENGTH_LONG).show();
								
				        
				        }
				    });
			}
			
		}catch(Exception e)
		{
			Log.e("exception is ",""+ e.toString());
			 Handler handler=new Handler();
			    handler=  new Handler(getActivity().getMainLooper());
			    handler.post( new Runnable(){
			        public void run(){
			        	try {
			        		progressGenerator.stop();
		        			btnSignIn.setEnabled(true);
		        			DatingApplication.getLoginAct().runOnUiThread( new Runnable() {
								public void run() {
									btnSignIn.setText("Sign In");
								}
							});
							Toast.makeText(getActivity(),obj1.getString("__imaniac_api__"),Toast.LENGTH_LONG).show();
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}catch(Exception e)
						{
							progressGenerator.stop();
		        			btnSignIn.setEnabled(true);
		        			DatingApplication.getLoginAct().runOnUiThread( new Runnable() {
								public void run() {
									btnSignIn.setText("Sign In");
								}
							});
							Toast.makeText(getActivity(),"Couldn't Connect to the server . Check Your Internet Connection",Toast.LENGTH_LONG).show();
						}
			        	/*if(sid!=null)
			        		Toast.makeText(getActivity(), sid,Toast.LENGTH_LONG).show();
						else
							
							{
								Toast.makeText(getActivity(), "Something Went Wrong ",Toast.LENGTH_LONG).show();
							}*/
			        
			        }
			    });
		}
		
		
		} 
	}
private class SendHttpPostTask extends AsyncTask<String, Void, String> 
{ 
	@Override 
	protected String doInBackground(String... params) 
	
	{ 
		String Url = "https://api.mOTP.in/v1/"+API_KEY+"/"+sid+"%20/%20-d%20"+PRIVATE_KEY;
		Log.e("post url is ", Url);
		
	    StringBuilder response = new StringBuilder();
	    try {
	        HttpPost post = new HttpPost();
	        post.setURI(new URI(Url));
	        
	        DefaultHttpClient httpClient = new DefaultHttpClient();
	        HttpResponse httpResponse = httpClient.execute(post);
	        if (httpResponse.getStatusLine().getStatusCode() == 200) {
	            Log.d("post", "HTTP POST succeeded");
	            HttpEntity messageEntity = httpResponse.getEntity();
	            InputStream is = messageEntity.getContent();
	            BufferedReader br = new BufferedReader(new InputStreamReader(is));
	            String line;
	            while ((line = br.readLine()) != null) {
	                response.append(line);
	            }
	        } else {
	            Log.e("post", "HTTP POST status code is not 200");
	        }
	    } catch (Exception e) {
	        Log.e("post", e.getMessage());
	    }
	    Log.d("post", "Done with HTTP posting");
	    Log.e("post resonse is ", response.toString());
	    return response.toString();
	

		 //return null;
	} 
	@Override
	protected void onPostExecute(String data) { 
		} 
	}
public static void deleteLastCallLog(Context context, String phoneNumber) {

    try {
        //Thread.sleep(4000);
        String strNumberOne[] = { phoneNumber };
        Cursor cursor = context.getContentResolver().query(
                CallLog.Calls.CONTENT_URI, null,
                CallLog.Calls.NUMBER + " = ? ", strNumberOne, CallLog.Calls.DATE + " DESC");

        if (cursor.moveToFirst()) {
                int idOfRowToDelete = cursor.getInt(cursor.getColumnIndex(CallLog.Calls._ID));                      
                int foo = context.getContentResolver().delete(
                        CallLog.Calls.CONTENT_URI,
                        CallLog.Calls._ID + " = ? ",
                        new String[] { String.valueOf(idOfRowToDelete) });

        }
    } catch (Exception ex) {
        Log.v("deleteNumber",
                "Exception, unable to remove # from call log: "
                        + ex.toString());
    }
}
public static void fragment_set()
{
	sign_up.performClick();
}

}
