package com.imaniac.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.GpsStatus.NmeaListener;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.imaniac.api.GetFriends;
import com.imaniac.api.UnapprovedFriends;
import com.imaniac.api.UnreadMessages;
import com.imaniac.application.DatingApplication;
import com.imaniac.dating.LoginActivity;
import com.imaniac.dating.MainActivity;
import com.imaniac.dating.MessagingActivity;
import com.imaniac.dating.R;
import com.imaniac.dating.UnapprovedFriendList;


public class OnlineService extends Service{
	String status;
	public static OnlineService mContext;
	//IBinder mBinder = new LocalBinder();
	private NotificationManager mNM;
	public ConnectivityManager conManager = null; 
	private Timer timer;
	private final int UPDATE_TIME_PERIOD = 5000;
	public static String friend_uid,friend_name;
	public static int noti_count=0;
	/*@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return mBinder;
	}
	public class LocalBinder extends Binder {
		  public OnlineService getServerInstance() {
		   return OnlineService.this;
		  }
		 }*/
	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		Log.e("service", "created");
		mContext=this;
		 mNM = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
		 conManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
		 timer = new Timer();   
		 /*try{
		 GPSTracker gpsTracker = new GPSTracker(getApplicationContext());
			if (gpsTracker.canGetLocation())
	        {
	            String latitude = String.valueOf(gpsTracker.latitude);
	           
	            String longitude = String.valueOf(gpsTracker.longitude);
	            DatingApplication.e.putString("latitide", latitude);
	            DatingApplication.e.putString("longitude", longitude);
	            DatingApplication.e.commit();
	            Log.e("lat "+latitude, "long "+longitude);
	            Log.e("postal", ""+gpsTracker.getAddressLine(getApplicationContext()));
	        }
	}catch(Exception e)
	{
		
	}*/
		 timer.schedule(new TimerTask()
			{			
				public void run() 
				{
					if(DatingApplication.getAct()!=null)
					{
					/*try {				
						//rawFriendList = IMService.this.getFriendList();
						// sending friend list 
						DatingApplication.getAct().runOnUiThread(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								
								
								
								
								
								
								ConnectivityManager cm =
								        (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
								    NetworkInfo netInfo = cm.getActiveNetworkInfo();
								    if (netInfo != null && netInfo.isConnectedOrConnecting()&& !DatingApplication.sp.getString("uid", "logout").equalsIgnoreCase("logout")) {
								    	GetFriends frnd=new GetFriends();
										try {
											frnd.search("abc");
										} catch (ClientProtocolException e1) {
											// TODO Auto-generated catch block
											e1.printStackTrace();
										} catch (IOException e1) {
											// TODO Auto-generated catch block
											e1.printStackTrace();
										} catch (URISyntaxException e1) {
											// TODO Auto-generated catch block
											e1.printStackTrace();
										} catch (JSONException e1) {
											// TODO Auto-generated catch block
											e1.printStackTrace();
										}
										DatingApplication.getAct().update_friends();
										UnapprovedFriends uf=new UnapprovedFriends();
										UnreadMessages um = new UnreadMessages();
										try {
											uf.search("BC");
											um.search("abc");
										} catch (ClientProtocolException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										} catch (IOException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										} catch (URISyntaxException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										} catch (JSONException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}catch(Exception e)
										{
											//Log.e("Service", "null");
										}
								    }
								    
								
							}
						});
						
						//check_unapproved_friends();
					}
					catch (Exception e) {
						e.printStackTrace();
					}		*/
						new UpdateRegularly().execute();
				}	
				}
			}, UPDATE_TIME_PERIOD, UPDATE_TIME_PERIOD);
		 
		 
		 
		
	        
	}
	public void setasregistered(String on_stat)
	{
		status=on_stat;
		new SignInTask().execute();
	}
	public void change_friend_status(String friend,String stat)
	{
		 String status[] ={friend,stat};
		new ChangeFriendApproval().execute(status);
	}
	
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		stopSelf();
		Log.e("service", "destroyed");
		
	}
	private class UpdateRegularly extends AsyncTask<String, Void, String>
	{
		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
		/*try{	
			 GPSTracker gpsTracker = new GPSTracker(getApplicationContext());
				if (gpsTracker.canGetLocation())
		        {
		            String latitude = String.valueOf(gpsTracker.latitude);
		           
		            String longitude = String.valueOf(gpsTracker.longitude);
		            DatingApplication.e.putString("latitide", latitude);
		            DatingApplication.e.putString("longitude", longitude);
		            DatingApplication.e.commit();
		            Log.e("lat "+latitude, "long "+longitude);
		           // Log.e("postal", ""+gpsTracker.getAddressLine(getApplicationContext()));
		        }
		}catch(Exception e)
		{
			
		}*/
			ConnectivityManager cm =
			        (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			    NetworkInfo netInfo = cm.getActiveNetworkInfo();
			    if (netInfo != null && netInfo.isConnectedOrConnecting()&& !DatingApplication.sp.getString("uid", "logout").equalsIgnoreCase("logout")) {
			    	GetFriends frnd=new GetFriends();
					try {
						frnd.search("abc");
					} catch (ClientProtocolException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (URISyntaxException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (JSONException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					DatingApplication.getAct().update_friends();
					UnapprovedFriends uf=new UnapprovedFriends();
					UnreadMessages um = new UnreadMessages();
					try {
						uf.search("BC");
						um.search("abc");
					} catch (ClientProtocolException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (URISyntaxException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}catch(Exception e)
					{
						//Log.e("Service", "null");
					}
			    }
			return null;
		}
	}
	private class SignInTask extends AsyncTask<String, Void, String> 
	{ 
		@Override 
		protected String doInBackground(String... params) 
		
		{ 
			String Url = "http://imaniacgroup.com/api/dating/updateUserProfile.php?uid="+DatingApplication.sp.getString("uid", "abc")+"&on_stat="+status;
			Log.e("sign url is ", Url);
			
		    StringBuilder response = new StringBuilder();
		    try {
		        HttpPost post = new HttpPost();
		        post.setURI(new URI(Url));
		        
		        DefaultHttpClient httpClient = new DefaultHttpClient();
		        HttpResponse httpResponse = httpClient.execute(post);
		        if (httpResponse.getStatusLine().getStatusCode() == 200) {
		            Log.d("post", "HTTP POST succeeded");
		            HttpEntity messageEntity = httpResponse.getEntity();
		            InputStream is = messageEntity.getContent();
		            BufferedReader br = new BufferedReader(new InputStreamReader(is));
		            String line;
		            while ((line = br.readLine()) != null) {
		                response.append(line);
		            }
		        } else {
		            Log.e("post", "HTTP POST status code is not 200 but "+httpResponse.getStatusLine().getStatusCode());
		        }
		    } catch (Exception e) {
		        Log.e("post", e.getMessage());
		    }
		    Log.d("post", "Done with HTTP posting");
		    Log.e("post resonse is ", response.toString());
		    
		   
		   // return response.toString();
		

			 return null;
		} 
		@Override
		protected void onPostExecute(String data) { 
		}
		}
	
	private class ChangeFriendApproval extends AsyncTask<String, Void, String> 
	{ 
		@Override 
		protected String doInBackground(String... params) 
		
		{ 
			String Url = "http://imaniacgroup.com/api/dating/approveFriendStatus.php?from="+params[0]+"&to="+DatingApplication.sp.getString("uid", "abc")+"&status="+params[1];
			Log.e("friend url is ", Url);
			
		    StringBuilder response = new StringBuilder();
		    try {
		        HttpPost post = new HttpPost();
		        post.setURI(new URI(Url));
		        
		        DefaultHttpClient httpClient = new DefaultHttpClient();
		        HttpResponse httpResponse = httpClient.execute(post);
		        if (httpResponse.getStatusLine().getStatusCode() == 200) {
		            Log.d("post", "HTTP POST succeeded");
		            HttpEntity messageEntity = httpResponse.getEntity();
		            InputStream is = messageEntity.getContent();
		            BufferedReader br = new BufferedReader(new InputStreamReader(is));
		            String line;
		            while ((line = br.readLine()) != null) {
		                response.append(line);
		            }
		        } else {
		            Log.e("post", "HTTP POST status code is not 200 but "+httpResponse.getStatusLine().getStatusCode());
		        }
		    } catch (Exception e) {
		        Log.e("post", e.getMessage());
		    }
		    Log.d("post", "Done with HTTP posting");
		    Log.e("post resonse is ", response.toString());
		    
		   
		   // return response.toString();
		

			 return null;
		} 
		@Override
		protected void onPostExecute(String data) { 
		}
		}
	public static OnlineService getService() {
		// TODO Auto-generated method stub
		return mContext;
	}
	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}
	public void restart()
	{
		Toast.makeText(getApplicationContext(), "Sign Up Successful", 1000).show();
		
		startActivity(new Intent(getApplicationContext(),LoginActivity.class));
	}
	
	
	
	
	 public void showFriendNotification() 
		{       
	        // Set the icon, scrolling text and TIMESTAMP
	    	String title = "Dating: You got a new Friend Request! ";
	 				
	    	
	    	
	    	//NotificationCompat.Builder notification = new NotificationCompat.Builder(R.drawable.stat_sample, title,System.currentTimeMillis());
	    	NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
	    	.setSmallIcon(R.drawable.button4)
	    	.setContentTitle(title);
	    	
	    	

	        Intent i = new Intent(this, UnapprovedFriendList.class);
	       
	        //i.putExtra(MessageInfo.MESSAGETEXT, msg);	
	        
	        // The PendingIntent to launch our activity if the user selects this notification
	        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
	                i, 0);

	        // Set the info for the views that show in the notification panel.
	        // msg.length()>15 ? MSG : msg.substring(0, 15);
	        mBuilder.setContentIntent(contentIntent); 
	        
	        mBuilder.setContentText("New friend request " );
	        
	        //TODO: it can be improved, for instance message coming from same user may be concatenated 
	        // next version
	        mNM=(NotificationManager) getSystemService(getApplicationContext().NOTIFICATION_SERVICE);
	        // Send the notification.
	        // We use a layout id because it is a unique number.  We use it later to cancel.
	        mNM.notify(300, mBuilder.build());
	    }
	 
	
	
	
	
	
	 public void showNotification(String username,String uid, String msg) 
		{       
	        // Set the icon, scrolling text and TIMESTAMP
	    	String title = "Dating: You got a new Message! (" + username + ")";
	 				
	    	String text = username + ": " + 
	     				((msg.length() < 5) ? msg : msg.substring(0, 5)+ "...");
	    	
	    	//NotificationCompat.Builder notification = new NotificationCompat.Builder(R.drawable.stat_sample, title,System.currentTimeMillis());
	    	NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
	    	.setSmallIcon(R.drawable.button4)
	    	.setContentTitle(title)
	    	.setContentText(text); 
	    	
	    	

	        Intent i = new Intent(this, MainActivity.class);
	        i.putExtra("friend", uid);
	        i.putExtra("name", username);
	        Log.e("sending intent", username+uid);
	        //i.putExtra(MessageInfo.MESSAGETEXT, msg);	
	        
	        // The PendingIntent to launch our activity if the user selects this notification
	        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
	                i, 0);

	        // Set the info for the views that show in the notification panel.
	        // msg.length()>15 ? MSG : msg.substring(0, 15);
	        mBuilder.setContentIntent(contentIntent); 
	        mBuilder.setAutoCancel(true);
	        mBuilder.setOnlyAlertOnce(true);
	        noti_count++;
	        if(noti_count>1)
	        	mBuilder.setContentText(""+noti_count + " Unread Messages");
	        else
	        mBuilder.setContentText("Message from " + username + ": " + msg);
	        
	        //TODO: it can be improved, for instance message coming from same user may be concatenated 
	        // next version
	        mNM=(NotificationManager) getSystemService(getApplicationContext().NOTIFICATION_SERVICE);
	        // Send the notification.
	        // We use a layout id because it is a unique number.  We use it later to cancel.
	       // mNM.cancel(101);
	        
	        mNM.notify(101, mBuilder.build());
	    }
	 
	 public void print_toast(String s)
	 {
		 Toast.makeText(mContext, s, 2000).show();
	 }
		 
public void startMessaging()
{
	Intent i = new Intent(getApplicationContext(),MessagingActivity.class);
	i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	startActivity(i);
}
public Double distance (Double lat1,Double long1,Double lat2,Double long2)
{
	Double dlong =long2-long1;
	Double dlat=lat2-lat1;
	Double dist=Math.pow(Math.sin(Math.toRadians(dlat)),2)+Math.cos(Math.toRadians(lat1))*Math.cos(Math.toRadians(lat2))*Math.pow(Math.sin(Math.toRadians((dlong/2))),2);
	Double distance = 6373*2*(Math.atan2(Math.sqrt(dist),Math.sqrt(1-dist)));
	return distance;
}
}
