package com.imaniac.types;

public class FriendInfo {
	public String name;
	public String online_status;
	public String friend_status;
	//public String profile_status;
	public String uid;
	public String image_url;
	public String read;
	
	public FriendInfo(String name,String online_status,String friend_status,String uid,String image_url,String read) {
		// TODO Auto-generated constructor stub
		this.name=name;
		this.online_status=online_status;
		//this.profile_status=profile_status;
		this.friend_status=friend_status;
		this.uid=uid;
		this.image_url=image_url;
		this.read=read;
	}
	public String getFriend_status() {
		return friend_status;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getOnline_status() {
		return online_status;
	}
	public void setOnline_status(String online_status) {
		this.online_status = online_status;
	}
	/*public String getProfile_status() {
		return profile_status;
	}
	public void setProfile_status(String profile_status) {
		this.profile_status = profile_status;
	}*/
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public String getImage_url() {
		return image_url;
	}
	
	public String getRead() {
		return read;
	}
	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}
	
	

}
