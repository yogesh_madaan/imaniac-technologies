package com.imaniac.types;

public class WallMessage {
	String uid;
	String name;
	String msg;
	String metro;
	public WallMessage(String uid,String name,String msg,String metro) {
		// TODO Auto-generated constructor stub
		this.uid=uid;
		this.name=name;
		this.msg=msg;
		this.metro=metro;
	}
	public String getUid() {
		return uid;
	}
	public String getName() {
		return name;
	}
	public String getMsg() {
		return msg;
	}
	public String getMetro() {
		return metro;
	}
	

}
