package com.imaniac.utils;

import com.dd.processbutton.ProcessButton;
import com.imaniac.fragments.LoginFragment;

import android.os.Handler;
import android.util.Log;


import java.util.Random;

public class ProgressGenerator {

    public interface OnCompleteListener {

        public void onComplete(String status);
    }

    private OnCompleteListener mListener;
    public static int mProgress;
    Handler handler;
    boolean b;
    ProcessButton mButton;
    public ProgressGenerator(OnCompleteListener listener) {
        mListener = listener;
        b=true;
    }

    public void start(final ProcessButton button) {
        handler = new Handler();
        mButton=button;
        b=true;
        if(b)
        {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mProgress += 2;
                button.setProgress(mProgress);
               /* if (mProgress < 100) {
                    handler.postDelayed(this, generateDelay());
                } else {
                    mListener.onComplete();
                }*/
                /*if(mProgress>80 && mProgress<99 )
                	mProgress=10;*/
                if(LoginFragment.uid==null)
                {
                	if(b)
                	handler.postDelayed(this, generateDelay());
                	if(mProgress>80)
                		mProgress=2;
                	//Log.e("uid ", "null");
                }
                	
                else {
                	//Log.e("uid ", "not null");
                	mProgress=98;
                	if(b)
                		handler.postDelayed(this, generateDelay());
                  
                }
                
            }
        }, generateDelay());
        }
       
    }

    private Random random = new Random();

    private int generateDelay() {
        return random.nextInt(1000);
    }
    public void stop()
    {	Log.e("boolean", "false");
    	b=false;
//    	handler.removeCallbacks((Runnable) this);
    	handler.removeCallbacksAndMessages(null);
    	mProgress=0;
    	mButton.setProgress(mProgress);
    }
    
}
