package com.imaniac.databases;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.imaniac.application.DatingApplication;

public class LocalStorageHandler extends SQLiteOpenHelper {

	private static final String TAG = LocalStorageHandler.class.getSimpleName();
	
	private static final String DATABASE_NAME = "Dating.db";
	private static final int DATABASE_VERSION = 1;
	
	private static final String _ID = "_id";
	private static final String TABLE_NAME_MESSAGES = "messages";
	private static final String TABLE_NAME_FRIENDS = "friends";
	private static final String TABLE_NAME_FRIEND_REQUEST = "friend_request";
	public static final String MESSAGE_RECEIVER = "receiver";
	public static final String MESSAGE_RECEIVER_NAME = "receivername";
	public static final String MESSAGE_SENDER = "sender";
	public static final String MESSAGE_SENDER_NAME = "sendername";
	private static final String MESSAGE_MESSAGE = "message";
	public static final String FRIENDS_NAME = "friend_name";
	public static final String FRIENDS_DESTINATION = "dest";
	public static final String STATUS = "status";
	public static final String READ_STATUS = "readit";
	public static final String FRIENDS_UID = "friend_uid";
	
	
	
	private static final String TABLE_MESSAGE_CREATE
	= "CREATE TABLE " + TABLE_NAME_MESSAGES
	+ " (" + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
	+ MESSAGE_RECEIVER + " VARCHAR(32), "
	+ MESSAGE_SENDER + " VARCHAR(32), "
	+ MESSAGE_RECEIVER_NAME + " VARCHAR(32), "
	+ MESSAGE_SENDER_NAME + " VARCHAR(32), "
	+MESSAGE_MESSAGE + " VARCHAR(255), "
	+READ_STATUS + " VARCHAR(255));";
	
	private static final String TABLE_MESSAGE_DROP = 
			"DROP TABLE IF EXISTS "
			+ TABLE_NAME_MESSAGES;
	
	private static final String TABLE_FRIENDS_CREATE
	= "CREATE TABLE " + TABLE_NAME_FRIENDS
	+ " (" + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
	+ FRIENDS_NAME + " VARCHAR(32), "
	+ FRIENDS_DESTINATION + " VARCHAR(32));";
	
	private static final String TABLE_FRIENDS_DROP = 
			"DROP TABLE IF EXISTS "
			+ TABLE_NAME_FRIENDS;
	
	private static final String TABLE_FRIENDS_REQUEST
	= "CREATE TABLE " + TABLE_NAME_FRIEND_REQUEST
	+ " (" + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
	+ FRIENDS_UID + " VARCHAR(32));";
	
	private static final String TABLE_FRIENDS_REQUEST_DROP = 
			"DROP TABLE IF EXISTS "
			+ TABLE_NAME_FRIENDS;
	
	
	public LocalStorageHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(TABLE_MESSAGE_CREATE);
		db.execSQL(TABLE_FRIENDS_CREATE);
		db.execSQL(TABLE_FRIENDS_REQUEST);
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(TAG, "Upgrade der DB von V: "+ oldVersion + " zu V:" + newVersion + "; Alle Daten werden gel�scht!");
		db.execSQL(TABLE_MESSAGE_DROP);
		db.execSQL(TABLE_FRIENDS_DROP);
		db.execSQL(TABLE_FRIENDS_REQUEST);
		onCreate(db);
		
	}
	
	public void insert(String sender, String sender_name,String receiver,String receiver_name, String message){
		long rowId = -1;
		try{
			
			SQLiteDatabase db = getWritableDatabase();
			ContentValues values = new ContentValues();
			values.put(MESSAGE_RECEIVER, receiver);
			values.put(MESSAGE_SENDER, sender);

			values.put(MESSAGE_RECEIVER_NAME, receiver_name);
			values.put(MESSAGE_SENDER_NAME, sender_name);
			values.put(MESSAGE_MESSAGE, message);
			values.put(READ_STATUS, "0");
			rowId = db.insert(TABLE_NAME_MESSAGES, null, values);
			
			
		} catch (SQLiteException e){
			Log.e(TAG, "insert()", e);
		} finally {
			Log.d(TAG, "insert(): rowId=" + rowId);
		}
		
	}
	
	public void insert_friend(String orig,String dest)
	{
		long rowId=-1;
		try{
			
			SQLiteDatabase db = getWritableDatabase();
			ContentValues values = new ContentValues();
			values.put(FRIENDS_NAME, orig);
			values.put(FRIENDS_DESTINATION, dest);
			db.delete(TABLE_NAME_FRIENDS, FRIENDS_DESTINATION+"='"+dest+"'",null);
			rowId = db.insert(TABLE_NAME_FRIENDS, null, values);
			
		} catch (SQLiteException e){
			Log.e(TAG, "insert()", e);
		} finally {
			Log.d(TAG, "insert(): rowId=" + rowId);
		}
	}
	public void insert_friend_request(String uid)
	{
		long rowId=-1;
		try{
			
			SQLiteDatabase db = getWritableDatabase();
			ContentValues values = new ContentValues();
			values.put(FRIENDS_UID, uid);
			rowId = db.insert(TABLE_NAME_FRIEND_REQUEST, null, values);
			
		} catch (SQLiteException e){
			Log.e(TAG, "insert()", e);
		} finally {
			Log.d(TAG, "insert(): rowId=" + rowId);
		}
	}
	
	public boolean check_friend_request(String uid) {
		
		SQLiteDatabase db = getWritableDatabase();
		String SELECT_QUERY = "SELECT * FROM " + TABLE_NAME_FRIEND_REQUEST + " WHERE " + FRIENDS_UID+ " = '" + uid +"'"+" ORDER BY " + _ID + " ASC";
		
		if(db.rawQuery(SELECT_QUERY,null).getCount()>0)
			{
				Log.e("friend status","true");
				return true;
			}
		else
			{
			Log.e("friend status","false");
				return false;
			}
		
		//db.query(TABLE_NAME_MESSAGES, null, MESSAGE_SENDER + " LIKE ? OR " + MESSAGE_SENDER + " LIKE ?", sender , null, null, _ID + " ASC");
	
}
	
	public Cursor get_friends() {
		
		SQLiteDatabase db = getWritableDatabase();
		String SELECT_QUERY = "SELECT * FROM " + TABLE_NAME_FRIENDS + " ORDER BY " + _ID + " DESC";
		return db.rawQuery(SELECT_QUERY,null);
		
		//return db.query(TABLE_NAME_MESSAGES, null, MESSAGE_SENDER + " LIKE ? OR " + MESSAGE_SENDER + " LIKE ?", sender , null, null, _ID + " ASC");
	
}
	public Cursor get(String sender, String receiver) {
					
			SQLiteDatabase db = getWritableDatabase();
			String SELECT_QUERY = "SELECT * FROM " + TABLE_NAME_MESSAGES + " WHERE " + MESSAGE_SENDER + " LIKE '" + sender + "' AND " + MESSAGE_RECEIVER + " LIKE '" + receiver + "' OR " + MESSAGE_SENDER + " LIKE '" + receiver + "' AND " + MESSAGE_RECEIVER + " LIKE '" + sender + "' ORDER BY " + _ID + " ASC";
			return db.rawQuery(SELECT_QUERY,null);
			
			//return db.query(TABLE_NAME_MESSAGES, null, MESSAGE_SENDER + " LIKE ? OR " + MESSAGE_SENDER + " LIKE ?", sender , null, null, _ID + " ASC");
		
	}
	public void change_read_status(String sender, String receiver) {
		
		SQLiteDatabase db = getWritableDatabase();
		String SELECT_QUERY = "UPDATE " + TABLE_NAME_MESSAGES + " SET "+READ_STATUS +"='1'"+" WHERE " + MESSAGE_SENDER + " = '" + sender + "'";
		
		Log.e("setting", SELECT_QUERY);
		db.rawQuery(SELECT_QUERY,null);
		Log.e("no of rows read changed", String.valueOf(db.rawQuery(SELECT_QUERY,null).getCount()));
		Cursor cursor = null;
		try
		{
		    cursor = db.rawQuery("SELECT changes() AS affected_row_count", null);
		    if(cursor != null && cursor.getCount() > 0 && cursor.moveToFirst())
		    {
		        final long affectedRowCount = cursor.getLong(cursor.getColumnIndex("affected_row_count"));
		        Log.e("LOG", "affectedRowCount = " + affectedRowCount);
		    }
		    else
		    {
		        // Some error occurred?
		    }
		}
		catch(SQLException e)
		{
		    // Handle exception here.
		}
		finally
		{
		    if(cursor != null)
		    {
		        cursor.close();
		    }
		}
		//return db.query(TABLE_NAME_MESSAGES, null, MESSAGE_SENDER + " LIKE ? OR " + MESSAGE_SENDER + " LIKE ?", sender , null, null, _ID + " ASC");
	
}
	public void check_unread_messages(String receiver) {
		
		SQLiteDatabase db = getWritableDatabase();
		String SELECT_QUERY = "SELECT * FROM " + TABLE_NAME_MESSAGES + " WHERE " + MESSAGE_RECEIVER + " = " + receiver +"' AND " +READ_STATUS + " LIKE '"+'0'+"' ORDER BY " + _ID + " ASC";
		if(db.rawQuery(SELECT_QUERY,null).getCount()>0)
			DatingApplication.getService().showNotification(DatingApplication.profile_uid,"abc","Abc");
		db.rawQuery(SELECT_QUERY,null);
		
		//db.query(TABLE_NAME_MESSAGES, null, MESSAGE_SENDER + " LIKE ? OR " + MESSAGE_SENDER + " LIKE ?", sender , null, null, _ID + " ASC");
	
}
	
public int check_unread_messages_specific(String sender) {
		
		SQLiteDatabase db = getWritableDatabase();
		String SELECT_QUERY = "SELECT * FROM " + TABLE_NAME_MESSAGES + " WHERE " + MESSAGE_SENDER + " = '" + sender +"' AND " +READ_STATUS + " = '"+'0'+ "'";
		
		db.rawQuery(SELECT_QUERY,null);
		Log.e("query", ""+db.rawQuery(SELECT_QUERY,null).getCount() + " "+SELECT_QUERY);
		Log.e("count retuening",String.valueOf(db.rawQuery(SELECT_QUERY,null).getCount()));
		return db.rawQuery(SELECT_QUERY,null).getCount();
		//return db.query(TABLE_NAME_MESSAGES, null, MESSAGE_SENDER + " LIKE ? OR " + MESSAGE_SENDER + " LIKE ?", sender , null, null, _ID + " ASC");
	
}
	
	

}
