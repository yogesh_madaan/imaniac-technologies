package com.imaniac.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.imaniac.dating.R;

public class FriendsListAdapter extends BaseAdapter{
	private Context mContext;
	public FriendsListAdapter(Context context) {
		// TODO Auto-generated constructor stub
		mContext=context;
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup arg2) {
		// TODO Auto-generated method stub
		LayoutInflater inflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	

		if (convertView == null) {


			
			
			convertView = inflater.inflate(R.layout.layout_friend_list_item, null);

		}
			ImageView friend_image=(ImageView)convertView.findViewById(R.id.image_friend);
			ImageView status = (ImageView)convertView.findViewById(R.id.image_online);
			TextView friend_name = (TextView)convertView.findViewById(R.id.friend_name);
			
			
		return convertView;
	}

}
