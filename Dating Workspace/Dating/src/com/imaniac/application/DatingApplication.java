package com.imaniac.application;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.imaniac.dating.LoginActivity;
import com.imaniac.dating.MainActivity;
import com.imaniac.dating.MessagingActivity;
import com.imaniac.dating.R;
import com.imaniac.service.OnlineService;
import com.imaniac.types.FriendInfo;




public class DatingApplication extends Application{
	public static MainActivity mActivity;
	public static LoginActivity mLogin;
	public static DatingApplication mInstance;
	public static OnlineService mService;
	public static MessagingActivity mMessage;
	public static SharedPreferences sp;
	public static Editor e;
	public static List<FriendInfo> approvedFriends;
	public static List<FriendInfo> unapprovedFriends;
	public static List<String> uids;
	public static String profile_uid;
	public static String view_profile_uid;
	private static final String PROPERTY_ID = "UA-47713832-12";
	public enum TrackerName {
	    APP_TRACKER, // Tracker used only in this app.
	    GLOBAL_TRACKER, // Tracker used by all the apps from a company. eg: roll-up tracking.
	     }

	  HashMap<TrackerName, Tracker> mTrackers = new HashMap<TrackerName, Tracker>();
	
	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		mActivity = new MainActivity();
		mLogin=new LoginActivity();
		mMessage=new MessagingActivity();
		mInstance=this;
		Intent musicservice = new Intent(this,OnlineService.class);
		startService(musicservice);
		approvedFriends=new ArrayList<FriendInfo>();
        unapprovedFriends=new ArrayList<FriendInfo>();
        uids=new ArrayList<String>();
		Log.e("application", "created");
		 sp=getSharedPreferences("com.imaniac.dating", Context.MODE_PRIVATE);
			e=sp.edit();
			profile_uid=sp.getString("uid", "abc");
	}
	@Override
	public void onTerminate() {
		// TODO Auto-generated method stub
		super.onTerminate();
		Intent musicservice = new Intent(this,OnlineService.class);
		stopService(musicservice);
	}
	public static MainActivity getAct()
	{
		return mActivity;
	}
	public static DatingApplication getApp()
	{
		return mInstance;
	}
	public void setAct(MainActivity mAct)
	{
		mActivity=mAct;
	}
	public static LoginActivity getLoginAct()
	{
		return mLogin;
	}
	
	public void setLoginAct(LoginActivity mAct)
	{
		mLogin=mAct;
	}
	public static MessagingActivity getMessageMAct()
	{
		return mMessage;
	}
	
	public void setMessageAct(MessagingActivity mAct)
	{
		mMessage=mAct;
	}
	public static OnlineService getService()
	{
		mService = OnlineService.getService();
		if( mService == null )
    		Log.e("NOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO", "No0b");
    	while(mService==null)
    		mService = OnlineService.getService();
    	return mService;
	}
	 public synchronized Tracker getTracker(TrackerName trackerId) {
		    if (!mTrackers.containsKey(trackerId)) {

		      GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
		      
		      Tracker t = (Tracker) ((trackerId == TrackerName.APP_TRACKER) ? analytics.newTracker(R.xml.app_tracker)
		          : (trackerId == TrackerName.GLOBAL_TRACKER)) ;
		      mTrackers.put(trackerId, t);

		    }
		    return mTrackers.get(trackerId);
		  }

}