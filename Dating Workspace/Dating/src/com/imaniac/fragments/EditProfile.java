package com.imaniac.fragments;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.dd.processbutton.iml.ActionProcessButton;
import com.iangclifton.android.floatlabel.FloatLabel;
import com.imaniac.application.DatingApplication;
import com.imaniac.dating.LoginActivity;
import com.imaniac.dating.R;


import com.imaniac.utils.ProgressGenerator;
import com.throrinstudio.android.common.libs.validator.validator.EmailValidator;
import com.throrinstudio.android.common.libs.validator.validator.NotEmptyValidator;
import com.throrinstudio.android.common.libs.validator.validator.PhoneValidator;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

public class EditProfile extends Fragment implements com.imaniac.utils.ProgressGenerator.OnCompleteListener ,OnItemSelectedListener {
	public static final String EXTRAS_ENDLESS_MODE = "EXTRAS_ENDLESS_MODE";
	public static final String API_KEY = "0004-4a7d38c1-536b4a7b-3093-3a849e81";
	public static final String PRIVATE_KEY = "private=0009-4a7d38c1-536b4a7b-309c-50c9d751";
	public static final String DOMAIN = "imaniac.org";
	String sid;
	JSONObject obj1,temp;
	public static ProgressDialog pDialog;
	public static  String uid;
	ViewGroup current_view;
	//Scene current,next;
	ImageView iv;
	ProgressGenerator progressGenerator;
	ActionProcessButton sign_up;
	FloatLabel username,password,age,phone,email,address;
	String Username,Password,Age,Phone,Email,Address,Gender,Relationship_Status;
	int gender_pos,relation_pos;
	Spinner gender,relationship;
	String gender_list[] = {"male","female","others"};
	int sign_up_menu=0;
	boolean v_name=false,v_password=false,v_email = false,v_address=false,v_phone=false,v_age=false;
	String relationship_list[] = {"single","committed","married","divorcee"};
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		ViewGroup rootView=(ViewGroup)inflater.inflate(R.layout.layout_profile,container,false);
		
current_view=(ViewGroup)rootView.findViewById(R.id.current_scene_layout);
		
		Gender=gender_list[0];
		Relationship_Status=relationship_list[0];
		iv=(ImageView)rootView.findViewById(R.id.profile_pic);
		username=(FloatLabel)rootView.findViewById(R.id.id_username);
		password=(FloatLabel)rootView.findViewById(R.id.id_password);
		age=(FloatLabel)rootView.findViewById(R.id.id_age);
		phone=(FloatLabel)rootView.findViewById(R.id.id_phone);
		email=(FloatLabel)rootView.findViewById(R.id.id_email);
		address=(FloatLabel)rootView.findViewById(R.id.id_address);
		gender=(Spinner)rootView.findViewById(R.id.id_gender);
		relationship=(Spinner)rootView.findViewById(R.id.id_relationship_status);
		sign_up=(ActionProcessButton)rootView.findViewById(R.id.btnSignUp);
		gender.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item,gender_list));
		relationship.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item,relationship_list));
		phone.setVisibility(View.GONE);
		gender.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				Gender=gender_list[arg2];
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		relationship.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				Relationship_Status=relationship_list[arg2];
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		progressGenerator= new ProgressGenerator(this);
		
		sign_up.setMode(ActionProcessButton.Mode.ENDLESS);
	
		sign_up.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//Toast.makeText(getActivity(), "clicked"+sign_up_menu, 1000).show();
				
					ConnectivityManager cm =
	        		        (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
	        		    NetworkInfo netInfo = cm.getActiveNetworkInfo();
	        		    if (netInfo != null && netInfo.isConnectedOrConnecting()) {
	        		    	Username=username.getEditText().getText().toString();
	    	            	Password=password.getEditText().getText().toString();
	    	            	Age=age.getEditText().getText().toString();
	    	            	Phone=phone.getEditText().getText().toString();
	    	            	Email=email.getEditText().getText().toString();
	    	            	Address=address.getEditText().getText().toString();
	    	            	Log.e("details", Username+Password+Age+Phone+Email+Address);
	    					validate();
	    					if(v_address&&v_age&&v_name&&v_email&&v_password)
	    						{
	    							progressGenerator.start(sign_up);
	    							Log.e("calllllled", "call");
	    							new CreateUserProfileTask().execute();
	    							//verify_phone_no();
	    						}
	    					else
	    						Log.e("notttttt", "called");
	        		    }
	        		    else
	        		    {
	        		    	Toast.makeText(getActivity(), "Not Connected to Internet", 2000).show();
	        		    }
				
					//progressGenerator.start(sign_up);
					//create_user_profile();
					
					
				
				
			}
		});
		
		new GetUserProfile().execute();
		try{ Timer timer=new Timer();
        timer.schedule(new TimerTask() {
				
				@Override
				public void run() {
					// TODO Auto-generated method stub
					
    			/*DatingApplication.getLoginAct().runOnUiThread( new Runnable() {
						public void run() {
							btnSignIn.setText("Sign In");
						}
					});*/
    			
						try{
							
							Toast.makeText(getActivity(), "Something Went Wrong ",Toast.LENGTH_LONG).show();
						}catch(Exception e)
						{
							
						}
					new GetUserProfile().cancel(true);
				}
			}, 30000);
		}catch(Exception e)
		{
			
		}
		return rootView;
	}
	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onComplete(String s) {
		// TODO Auto-generated method stub
		
	}
	public void setDetails()
	{
		username.getEditText().setText(Username);
		phone.getEditText().setText(Phone);
		age.getEditText().setText(Age);
		email.getEditText().setText(Email);
		address.getEditText().setText(Address);
		gender.setSelection(gender_pos);
		relationship.setSelection(relation_pos);
		
	}
	public void validate()
	{
		Log.e("validate function", "called");
		
		
		try{
			v_email= new EmailValidator(getActivity()).isValid(Email);
		}catch(Exception e)
		{
			
		}
		try{
			v_password= new NotEmptyValidator(getActivity()).isValid(Password);
		}catch(Exception e)
		{
			
		}
		try{
			v_name=new NotEmptyValidator(getActivity()).isValid(Username);
		}catch(Exception e)
		{
			
		}
		try{
			v_phone=new PhoneValidator(getActivity()).isValid(Phone);
		}catch(Exception e)
		{
			
		}
		try{
			v_address=new NotEmptyValidator(getActivity()).isValid(Address);
		}catch(Exception e)
		{
			
		}
		try{
			v_age = Integer.parseInt(Age)>18;
		}catch(Exception e)
		{
			
		}
		
		
		
		
		
		//Log.e("email validate", ""+v_email);
		if(!v_address)
			address.getEditText().setError("Address cannot be empty");
		if(!v_email)
			email.getEditText().setError("Email is not in correct format");
		if(!v_password)
			password.getEditText().setError("Password cannot be empty");
		/*if(!v_phone)
			address.getEditText().setError("Phone no is not in right format");*/
		if(!v_name)
			username.getEditText().setError("Name cannot be empty");
		if(!v_age)
			age.getEditText().setError("Age must be greater than 18");
		
	}
	
	private class CreateUserProfileTask extends AsyncTask<String, Void, String> 
	{

		

		

		@Override 
		protected String doInBackground(String... params) 
		
		{ 
			String U="http://imaniacgroup.com/api/dating/updateUserProfile.php?uid="+DatingApplication.sp.getString("uid", "abc")+"&name="+URLEncoder.encode(Username)+"&pass="+URLEncoder.encode(Password)+"&age="+URLEncoder.encode(Age)+"&gender="+URLEncoder.encode(Gender)+"&mstat="+URLEncoder.encode(Relationship_Status)+"&email="+URLEncoder.encode(Email)+"&address="+URLEncoder.encode(Address);
			Log.e("create string passing", U);
			HttpClient client=new DefaultHttpClient();
			HttpResponse response = null;
			URI url = null;
			try {
				url = new URI(U);
				HttpGet get=new HttpGet();
				get.setURI(url);
				try {
					response = client.execute(get);
				} catch (ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				HttpEntity entity=response.getEntity();
				try {
					String s=EntityUtils.toString(entity);
					Log.e("s is ",s);
					obj1 = new JSONObject(s);
					//temp= obj1.getJSONObject("__imaniac_api__");
					//Log.e("trial", obj1.getString("__imaniac_api__"));
					sid =obj1.getString("__imaniac_api__");
					Log.e("status received ", sid);
					
					
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}catch (Exception e)
			{
				DatingApplication.getAct().runOnUiThread(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						Log.e("sid is ", ""+sid);
						if(sid!=null)
			        		Toast.makeText(getActivity(), "Success",Toast.LENGTH_LONG).show();
						else
							try {
								if(obj1.getString("__imaniac_api__")!=null)
									{
									progressGenerator.stop();
				        			sign_up.setEnabled(true);
				        			sign_up.setText("Update");
										Toast.makeText(getActivity(),obj1.getString("__imaniac_api__") ,Toast.LENGTH_LONG).show(); 
									}
								else
									{
									progressGenerator.stop();
				        			sign_up.setEnabled(true);
				        			sign_up.setText("Update");
										Toast.makeText(getActivity(), "Something Went Wrong ",Toast.LENGTH_LONG).show();
									}
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (Exception e2) {
								// TODO: handle exception
								Log.e("exxxx",e2.toString());
							}
					}
				});
				 
			}
			
			
			
			
			
			
			
			

			 return null;
		} 
		@Override
		protected void onPostExecute(String data) { 
			DatingApplication.getAct().runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					// TODO Auto-generated method stub
					
					if(sid!=null)
		        		{
							Toast.makeText(getActivity(), sid,Toast.LENGTH_LONG).show();
							DatingApplication.getAct().switchContent(new ViewProfile());
		        		}
					
				}
			});
		}
	}
	
	private class GetUserProfileTask extends AsyncTask<String, Void, String> 
	{

		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			pDialog=new ProgressDialog(getActivity());
			pDialog.setTitle("Please Wait");
			// Set progressbar message
			pDialog.setMessage("Verifying your Mobile No ");
			pDialog.setIndeterminate(false);
			//pDialog.setCancelable(false);

			// Show progressbar
			//pDialog.show();
		} 
		

		@Override 
		protected String doInBackground(String... params) 
		
		{ 
			String U="http://imaniacgroup.com/api/dating/getUserProfile.php?uid="+DatingApplication.sp.getString("uid", "abc");
			Log.e("create string passing", U);
			HttpClient client=new DefaultHttpClient();
			HttpResponse response = null;
			URI url = null;
			try {
				url = new URI(URLEncoder.encode(U));
				HttpGet get=new HttpGet();
				get.setURI(url);
				try {
					response = client.execute(get);
				} catch (ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				HttpEntity entity=response.getEntity();
				try {
					String s=EntityUtils.toString(entity);
					Log.e("s is ",s);
					obj1 = new JSONObject(s);
					JSONArray array = null;
					try {
						array = obj1.getJSONArray("__imaniac_api__");
						JSONObject ob = array.getJSONObject(0);
						Log.e("json is ", ""+ob.toString());
						username.getEditText().setText(ob.getString("name"));
						age.getEditText().setText(ob.getString("age"));
						phone.getEditText().setText(ob.getString("phone"));
						email.getEditText().setText(ob.getString("email"));
						address.getEditText().setText(ob.getString("address"));
							
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}catch (Exception e)
			{
				Log.e("getting information", "exception "+e.toString());
				/* Handler handler=new Handler();
				    handler=  new Handler(getActivity().getMainLooper());
				    handler.post( new Runnable(){
				        public void run(){
				        	if(sid!=null)
				        		Toast.makeText(getActivity(), sid,Toast.LENGTH_LONG).show();
							else
								try {
									if(obj1.getString("__imaniac_api__")!=null)
										Toast.makeText(getActivity(),obj1.getString("__imaniac_api__") ,Toast.LENGTH_LONG).show(); 
									else
										Toast.makeText(getActivity(), "Something Went Wrong ",Toast.LENGTH_LONG).show();
								} catch (JSONException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								} 
				        }
				    });*/
				//Toast.makeText(getActivity(), "Something Went Wrong ", 2000).show();
				//DatingApplication.getAct().switchContent(new LoginFragment());
			}
			
			
			
			
			
			
			
			

			 return null;
		} 
		@Override
		protected void onPostExecute(String data) { 
			//verify_phone_no();
		}
	}
	
	private class GetUserProfile extends AsyncTask<String, Void, String> 
	{ 
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pDialog=new ProgressDialog(getActivity());
			pDialog.setTitle("Please Wait");
			// Set progressbar message
			pDialog.setMessage("Accessing User Profile ");
			pDialog.setIndeterminate(false);
			//pDialog.setCancelable(false);
			pDialog.setCanceledOnTouchOutside(false);
			// Show progressbar
			pDialog.show();
			
		} 
		
		@Override 
		protected String doInBackground(String... params) 
		
		{ 
			try{
			//Log.e("verfiy", "dialog");
			 String Url="http://imaniacgroup.com/api/dating/getUserProfile.php?uid="+DatingApplication.sp.getString("uid", "abc");
			
				URI url = null;
				try {
					url = new URI(Url);
				} catch (URISyntaxException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				HttpClient client=new DefaultHttpClient();
				
				HttpGet get=new HttpGet();
				get.setURI(url);
				
				HttpResponse response = null;
				try {
					response = client.execute(get);
				} catch (ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				HttpEntity entity=response.getEntity();
				String s = null;
				
					try {
						s=EntityUtils.toString(entity);
						Log.e("s ", s);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					JSONObject obj = null;
					try {
						obj = new JSONObject(s);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					JSONArray array = null;
					try {
						array = obj.getJSONArray("__imaniac_api__");
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						Log.e("exp", "called");
						e.printStackTrace();
					}
					
					

					for(int i=0;i<array.length();i++)
					{
						
						// Log.e("checking user profile", Url);
						try {
							JSONObject ob = array.getJSONObject(i);
							Log.e("ob is ", ob.toString());
							Username=ob.getString("name");
							Age=ob.getString("age");
							Phone=ob.getString("phone");
							Email=ob.getString("email");
							Address=ob.getString("address");
							gender_pos=Arrays.asList(gender_list).indexOf(ob.getString("gender"));

							relation_pos=Arrays.asList(relationship_list).indexOf(ob.getString("mstat"));
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							Log.e("json exception ", e.toString());
							e.printStackTrace();
						}
						
						
						
					}
					
					
					
					
			}catch(Exception e)
			{
				DatingApplication.getAct().runOnUiThread(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						DatingApplication.getService().print_toast("Seems Internet Not Working.Unable to fetch profile Details.");
						DatingApplication.getAct().switchContent(new WallFragment());
					}
				});
				
				Log.e("Exction profile","called"+e.toString());
			}

			 return null;
		} 
		@Override
		protected void onPostExecute(String data) { 
			pDialog.hide();
			setDetails();
		}
	}


}
