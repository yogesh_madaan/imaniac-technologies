package com.imaniac.fragments;

import java.io.FileNotFoundException;
import java.io.IOException;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.MediaStore.Images.Media;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.imaniac.application.DatingApplication;
import com.imaniac.dating.MainActivity;
import com.imaniac.dating.R;
import com.imaniac.dating.UnapprovedFriendList;

public class SampleListFragment extends Fragment implements OnItemClickListener {
	ListView mListView;
	Fragment fragment;
	ImageView mImage;
	TextView name,email;
	ImageButton edit;
	private static Bitmap Image=null;
	private static Bitmap rotateImage = null;
	public static int[] navigation_icons ={R.drawable.icon_home,R.drawable.icon_connection,R.drawable.icon_chat,R.drawable.icon_desc,R.drawable.icon_about,R.drawable.icon_unappfriends,R.drawable.icon_logout};
	public static String[] navigation_item_list = {"Home" , "Connections" , "Chat History", "Description" , "About Us","Unapproved Friends","Logout"};
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		ViewGroup rootView = (ViewGroup)inflater.inflate(R.layout.layout_navigation_list, container,false);
		mListView= (ListView)rootView.findViewById(R.id.listView1);
		mImage = (ImageView)rootView.findViewById(R.id.item_image);
		name = (TextView)rootView.findViewById(R.id.item_name);
		email = (TextView)rootView.findViewById(R.id.item_date);
		edit=(ImageButton)rootView.findViewById(R.id.btnEdit);
		edit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				DatingApplication.getAct().switchContent(new EditProfile());
			}
		});
		RelativeLayout rl =(RelativeLayout)rootView.findViewById(R.id.relativeLayout1);
		rl.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				DatingApplication.profile_uid=DatingApplication.sp.getString("uid", "abc");
				DatingApplication.view_profile_uid=DatingApplication.sp.getString("uid", "abc");
				DatingApplication.getAct().switchContent(new ViewProfile());
			}
		});
		mImage.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				DatingApplication.getAct().switchContent(new EditProfile());
			}
		});
		return rootView;
		
		
	}

	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		name.setText(MainActivity.sp.getString("name", "Guest"));
		email.setText(MainActivity.sp.getString("phone", ""));
		//set_image();
		mListView.setAdapter(new SampleAdapter(getActivity(), navigation_item_list));
		mListView.setOnItemClickListener(this);
	}

	

	public class SampleAdapter extends BaseAdapter {
		private Context mContext;
		String[] names;
	

		public SampleAdapter(Context context , String[] item_name ) {
			mContext=context;
			names = item_name;
			
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			LayoutInflater inflater = (LayoutInflater) mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			if (convertView == null) {
				convertView = inflater.inflate(R.layout.row, null);
			}
			ImageView icon = (ImageView) convertView.findViewById(R.id.row_icon);
			icon.setImageResource(navigation_icons[position]);
			TextView title = (TextView) convertView.findViewById(R.id.row_title);
			title.setText(names[position]);
			

			return convertView;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return names.length;
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return 0;
		}

	}



	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		switch(arg2){
		case 0 :
			DatingApplication.getAct().switchContent(new WallFragment());
			break;
		case 1 :
			DatingApplication.getAct().switchContent(new Connections());
			break;
		case 2 :
			DatingApplication.getAct().switchContent(new ChatFragment());
			break;
		case 3:
			 new AlertDialog.Builder(getActivity())
     	    .setTitle("About Developer:")
     	    .setMessage("Dating is a platform" +
     	    		"to interact with others" +"\n"+
     	    		" without knowing them personally " +
     	    		"It provides Real Time Chatting " +"\n"+"\n"+
     	    		"Near By Users" +"\n").show();
        	 break;
		case 4:
			 new AlertDialog.Builder(getActivity())
     	    .setTitle("About Developer:")
     	    .setMessage("iManiac Technologies is a website " +
     	    		"and mobile application development company." +"\n"+
     	    		" We are  providing IT services to a range of " +
     	    		"well reputed clients. " +"\n"+"\n"+
     	    		"Website:" +"\n"+
     	    		"www.imaniac.org" +"\n"+"\n"+
     	    		"Email ID:" +"\n"+
     	    		"info@imaniac.org" +"\n"+"\n"+
     	    		"Contact:" +"\n"+
     	    		"+91-9873390875, \n"+"+91-9711926404").show();
        	 break;
		case 5:
			Intent i =new Intent(getActivity(),UnapprovedFriendList.class);
			startActivity(i);
			break;
		case 6:
			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
					MainActivity.mContext);
	 
				// set title
				alertDialogBuilder.setTitle("Dating");
	 
				// set dialog message
				alertDialogBuilder
					.setMessage("Do you Really Want To Logout?")
					.setCancelable(false)
					.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							getActivity().finish();
							DatingApplication.e.putString("uid", "logout");
							DatingApplication.e.commit();
						}
					  })
					.setNegativeButton("No",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							// if this button is clicked, just close
							// the dialog box and do nothing
							//DatingApplication.getService().change_friend_status(uid, "rejected");
							dialog.cancel();
						}
					});
	 
					// create alert dialog
					AlertDialog alertDialog = alertDialogBuilder.create();
	 
					// show it
					alertDialog.show();
			
			break;
		}
		/*case 1:
			fragment = new Profile();
			break;
		case 2:
			fragment = new FakeCall();
			
			break;
		case 3:
			fragment= new FakeMessage();
			
			break;
		case 4:
			fragment = new FakeCallLog();
			
			break;
		case 5:
			fragment = new FakeMessageLog();
			
			break;
		case 6 :
       	 Intent sendIntent = new Intent();
       	 sendIntent.setAction(Intent.ACTION_SEND);
       	 sendIntent.putExtra(Intent.EXTRA_TEXT, "Hi I am using Paperpedia - The complete guide to previous year papers of different universities .");
       	 sendIntent.setType("text/plain");
       	 startActivity(sendIntent);
       	 break;
		case 9:
			 new AlertDialog.Builder(getActivity())
      	    .setTitle("About Developer:")
      	    .setMessage("iManiac Technologies is a website " +
      	    		"and mobile application development company." +"\n"+
      	    		" We are  providing IT services to a range of " +
      	    		"well reputed clients. " +"\n"+"\n"+
      	    		"Website:" +"\n"+
      	    		"www.imaniac.org" +"\n"+"\n"+
      	    		"Email ID:" +"\n"+
      	    		"info@imaniac.org" +"\n"+"\n"+
      	    		"Contact:" +"\n"+
      	    		"+91-9873390875, \n"+"+91-9711926404").show();
         	 break;
		case 7 :
			fragment = new ColorFragment();
			break;
		case 8:
			fragment = new ColorFragment();
			break;
         	 
		}
		if(arg2!=9)
		{
			getActivity().getSupportFragmentManager()
			.beginTransaction()
			.replace(R.id.content_frame, fragment)
			.commit();
			((SlidingFragmentActivity) MainActivity.mContext).getSlidingMenu().showContent();
				//Toast.makeText(getActivity(), "Clicked" +arg2, 1000).show(); 
				MainActivity.set_fragment(new FakeMessageLog());
		}
		*/
		
	}
	
	public void set_image()
	{
		Log.e("set", "setting");
		Uri mImageUri = Uri.parse(MainActivity.sp.getString("pic", ""));				
		try {
			
			Image = Media.getBitmap(getActivity().getContentResolver(), mImageUri);
			if (getOrientation(MainActivity.mContext, mImageUri) != 0) {
				Matrix matrix = new Matrix();
				matrix.postRotate(getOrientation(MainActivity.mContext, mImageUri));
				if (rotateImage != null)
					rotateImage.recycle();
				rotateImage = Bitmap.createBitmap(Image, 0, 0, Image.getWidth(), Image.getHeight(), matrix,
						true);
				mImage.setImageBitmap(rotateImage);
			} else
				mImage.setImageBitmap(Image);				
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch(Exception e)
		{
			mImage.setImageResource(R.drawable.ic_launcher);
		}
	
	}
	public static int getOrientation(Context context, Uri photoUri) {
		/* it's on the external media. */
		Cursor cursor = context.getContentResolver().query(photoUri,
				new String[] { MediaStore.Images.ImageColumns.ORIENTATION }, null, null, null);

		if (cursor.getCount() != 1) {
			return -1;
		}

		cursor.moveToFirst();
		return cursor.getInt(0);
	}
	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mMessageReceiver1,new IntentFilter("image"));
		
	}
	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		 LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mMessageReceiver1);
	}
	private BroadcastReceiver mMessageReceiver1 = new BroadcastReceiver() {
    	  @Override
    	  public void onReceive(Context context, Intent intent) {
    	    // Extract data included in the Intent
    	    String message = intent.getStringExtra("fragment");
    	    Log.e("receiver", "called");
    	    
    	    name.setText(MainActivity.sp.getString("name", "Guest"));
    		email.setText(MainActivity.sp.getString("email", ""));
    		set_image();
    	    
    	  }
    	};
}
