package com.imaniac.fragments;

import java.util.ArrayList;
import java.util.List;

import com.imaniac.application.DatingApplication;
import com.imaniac.dating.R;
import com.imaniac.fragments.SampleListFragment2.SampleItem;

import com.imaniac.service.OnlineService;
import com.imaniac.types.FriendInfo;
import com.imaniac.types.UserInfo;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.TextView;

public class Connections extends Fragment{
	private GridView gridview;
	SampleAdapter adapter;
	List<FriendInfo> ll = new ArrayList<FriendInfo>();
	public static int mPosition;
	TextView default_message;
	ViewGroup rootView;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		rootView=(ViewGroup)inflater.inflate(R.layout.layout_discover, container,false);
		gridview = (GridView)rootView.findViewById(R.id.gridView1);
		default_message = (TextView)rootView.findViewById(R.id.textView1);
		default_message.setText("Loading Connection.. ");
		return rootView;
		
	}
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		try{
				adapter= new SampleAdapter(getActivity());
				default_message.setText("No Connections Yet.");

			ll=DatingApplication.approvedFriends;
			for (int i = 0; i<ll.size(); i++) {
				adapter.add(new SampleItem(ll.get(i).getName(), android.R.drawable.ic_menu_search));
			}
			if(ll.size()==0)
			{
				default_message.setText("No Connections Yet.");
				Log.e("connection", " View No Connection");
			}
			if(ll.size()>0)
				{
				default_message.setVisibility(View.GONE);
				Log.e("connection", " View No Connection Gone");
				}
			gridview.setAdapter(adapter);
			//rootView.invalidate();
		}catch(Exception e)
		{
			Log.e(getClass().getSimpleName(), ""+e);
		}
	}
	public class SampleItem {
		public String tag;
		public int iconRes;
		public SampleItem(String tag, int iconRes) {
			this.tag = tag; 
			this.iconRes = iconRes;
		}
	}
	public class SampleAdapter extends ArrayAdapter<SampleItem> {

		public SampleAdapter(Context context) {
			super(context, 0);
		}

		public View getView(final int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				convertView = LayoutInflater.from(getContext()).inflate(R.layout.layout_discover_item, null);
			}
			TextView name = (TextView)convertView.findViewById(R.id.name);
			//name.setText(ll.get(position).getName());
			name.setText(getItem(position).tag);
			TextView details = (TextView)convertView.findViewById(R.id.details);
			details.setVisibility(View.GONE);
			ImageButton add=(ImageButton)convertView.findViewById(R.id.add_friend);
			add.setVisibility(View.GONE);
			mPosition=position;
			convertView.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					OnlineService.friend_name=ll.get(position).getName();
					OnlineService.friend_uid=ll.get(position).getUid();
					DatingApplication.getService().startMessaging();
				}
			});
			return convertView;
		}

	}
	
}
