package com.imaniac.fragments;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.imaniac.dating.R;

public class ImageFragment extends Fragment {
	private int[] IMAGE_IDS = {
			R.drawable.splash0, R.drawable.splash1, R.drawable.splash2,
			R.drawable.splash3,R.drawable.splash4
		};
	int mPosition;
    public ImageFragment(int position) {
		// TODO Auto-generated constructor stub
    	mPosition=position;
	}
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.layout_home_image, container, false);
        ImageView iv = (ImageView)rootView.findViewById(R.id.imageView1);
        Bitmap d = BitmapFactory.decodeResource(getResources(),IMAGE_IDS[mPosition]);
    	int nh = (int) ( d.getHeight() * (512.0 / d.getWidth()) );
    	Bitmap scaled = Bitmap.createScaledBitmap(d, 512, nh, true);
        iv.setImageBitmap(scaled);

        return rootView;
    }
}
