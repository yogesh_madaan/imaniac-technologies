package com.imaniac.fragments;

import java.util.List;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.imaniac.application.DatingApplication;
import com.imaniac.dating.R;
import com.imaniac.service.OnlineService;
import com.imaniac.types.FriendInfo;

public class SampleListFragment2 extends Fragment {
	List<FriendInfo> ll;
	static SampleAdapter adapter ;
	ListView lv;
	TextView tv;
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		ViewGroup rootView=(ViewGroup)inflater.inflate(R.layout.list, container,false);
		lv=(ListView)rootView.findViewById(R.id.list);
		tv=(TextView)rootView.findViewById(R.id.textView1);
		return rootView;
	}

	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		adapter= new SampleAdapter(getActivity());
		new SetFriends().execute();

		lv.invalidate();
		
		
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				OnlineService.friend_name=ll.get(arg2).getName();
				OnlineService.friend_uid=ll.get(arg2).getUid();
				DatingApplication.getService().startMessaging();
				//DatingApplication.getAct().switchContent(new MessagingA);
			}
		});
	}

	public class SampleItem {
		public String tag;
		public int iconRes;
		public SampleItem(String tag, int iconRes) {
			this.tag = tag; 
			this.iconRes = iconRes;
		}
	}

	public class SampleAdapter extends ArrayAdapter<SampleItem> {

		public SampleAdapter(Context context) {
			super(context, 0);
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				convertView = LayoutInflater.from(getContext()).inflate(R.layout.row, null);
			}
			ImageView online_icon=(ImageView)convertView.findViewById(R.id.online_icon);
			ImageView reg_icon=(ImageView)convertView.findViewById(R.id.reg_status);
			try{
				if(ll.get(position).getOnline_status().equalsIgnoreCase("online"))
			
				{
				//Log.e("deepak", "visibility changed");
					online_icon.setVisibility(View.VISIBLE);
				}
				if(ll.get(position).getFriend_status().equalsIgnoreCase("unregistered"))
				{
					reg_icon.setVisibility(View.VISIBLE);
					
				}
				
			ImageView icon = (ImageView) convertView.findViewById(R.id.row_icon);
			//icon.setImageResource(getItem(position).iconRes);
			TextView title = (TextView) convertView.findViewById(R.id.row_title);
			title.setText(getItem(position).tag);
			}catch(Exception e )
			{
				
			}
			return convertView;
		}

	}
	
	
	private class SetFriends extends AsyncTask<String, Void, String> 
	{ 
		@Override 
		protected String doInBackground(String... params) 
		
		{ 
			
			try{
				ll=DatingApplication.approvedFriends;
			}catch(Exception e)
			{
				Log.e("fetching approved list", ""+e.toString());
			}
		

			 return null;
		} 
		@Override
		protected void onPostExecute(String data) { 
			try{
				adapter= new SampleAdapter(getActivity());
			for (int i = 0; i<ll.size(); i++) {
				adapter.add(new SampleItem(ll.get(i).getName(), android.R.drawable.ic_menu_search));
				if(ll.size()==0)
				{
					setDefault();
				}
				else
				{
					try{
						tv.setVisibility(View.GONE);
					}catch(Exception e)
					{
						//Log.e(getClass().getSimpleName(), "view exception"+e);
					}
				}
			}
			lv.setAdapter(adapter);
		}catch(Exception e)
		{
			tv.setVisibility(View.VISIBLE);
			Log.e(getClass().getSimpleName(), "view visible exception"+e);
		}
	}
	public void setDefault()
	{
		try{
			//tv.setVisibility(View.VISIBLE);
		}catch(Exception e)
		{
			
		}
	}
	}
	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mMessageReceiver1,
			      new IntentFilter("visibility"));
	}
	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mMessageReceiver1);
	}
	private BroadcastReceiver mMessageReceiver1 = new BroadcastReceiver() {
  	  @Override
  	  public void onReceive(Context context, Intent intent) {
  	    // Extract data included in the Intent
  	    String message = intent.getStringExtra("fragment");
  	  // Log.e("receiver", "called");
  	   new SetFriends().execute();
  	    
  	   
  	    
  	  }
  	};
	
}
