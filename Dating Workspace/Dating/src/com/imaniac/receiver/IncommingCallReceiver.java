package com.imaniac.receiver;

import java.util.Timer;
import java.util.TimerTask;

import com.imaniac.fragments.LoginFragment;
import com.imaniac.fragments.WallFragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

public class IncommingCallReceiver extends BroadcastReceiver
{
   
     static boolean ring=false;
     static boolean callReceived=false;
     String callerPhoneNumber;
     public static String no;
     
     
     
     @Override
     public void onReceive(final Context mContext, Intent intent)
     {
         
            // Get the current Phone State
           String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
           
           if(state==null)
               return;

           // If phone state "Rininging"
           if(state.equals(TelephonyManager.EXTRA_STATE_RINGING))
           {           
                     ring =true;
                    // Get the Caller's Phone Number
                    Bundle bundle = intent.getExtras();
                    callerPhoneNumber= bundle.getString("incoming_number");
                    Log.e("call from", ""+callerPhoneNumber);
                    no=callerPhoneNumber;
             }



            // If incoming call is received
           if(state.equals(TelephonyManager.EXTRA_STATE_OFFHOOK))
            {
                   callReceived=true;
             }


            // If phone is Idle
           if (state.equals(TelephonyManager.EXTRA_STATE_IDLE))
           {
                     // If phone was ringing(ring=true) and not received(callReceived=false) , then it is a missed call
                      if(ring==true&&callReceived==false)
                      {
                               //Toast.makeText(mContext, "It was A MISSED CALL from : "+no, Toast.LENGTH_LONG).show();
                               if(no.contains("1234567"))
                               {
                            	   WallFragment.pDialog.dismiss();
                               		Timer timer = new Timer();
                               		timer.schedule(new TimerTask() {
										
										@Override
										public void run() {
											// TODO Auto-generated method stub
											 WallFragment.deleteLastCallLog(mContext, no);
										}
									}, 5000);
                            	  
                               }
                      }
         }
       
     }
}
