package com.imaniac.types;

public class MessageInfo {
	public String name;
	public String msg;
	//public String profile_status;
	public String uid;
	public MessageInfo(String msg,String uid,String name) {
		// TODO Auto-generated constructor stub
		this.msg=msg;
		this.uid=uid;
		this.name=name;
	}
	public String getName() {
		return name;
	}
	public String getMsg() {
		return msg;
	}
	public String getUid() {
		return uid;
	}
	

}
