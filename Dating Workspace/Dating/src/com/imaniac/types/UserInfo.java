package com.imaniac.types;

public class UserInfo {
	public String name;
	public String uid;
	public String age;
	
	public UserInfo(String name,String uid,String age) {
		// TODO Auto-generated constructor stub
		this.name=name;
		
		this.uid=uid;
		this.age=age;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	
	

}
